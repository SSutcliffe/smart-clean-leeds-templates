$(function() {
    $( '.gallery-holder' ).cycle({
        speed: 1000,
        slides: '.gallery-slide',
        fx: 'carousel',
        pager: '.gallery-pager',
        carouselVisible: 1,
        carouselFluid: true,
        pagerTemplate: '<a class="pager-item" href=""></a>',
    });
});

$(function() {
    $( '.carousel' ).cycle({
        speed: 1000,
        slides: '.testimonal',
        fx: 'carousel',
        next: '.next',
        prev: '.prev',
        carouselVisible: 1,
        carouselFluid: true,
    });
});
