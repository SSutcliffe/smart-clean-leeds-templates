(function($) {

$(document).ready(function() {
    $('.nav-toggle, .nav-overlay').on('click', function(){
       $(".nav").toggleClass('-closed');
       $(".nav-overlay").toggleClass('-closed');
    });
});
})(jQuery);

function em2px(input) {
    var emSize = parseFloat($("body").css("font-size"));
    return (emSize * input);
}

function menuClass() {

    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    if (width <= em2px(44.44)) {
        $(".nav").addClass("-closed");
        $(".nav-overlay").addClass('-closed');
    }
    else {
        $(".nav").removeClass("-closed");
        $(".nav-overlay").removeClass('-closed');
    }
}

$(window).resize(function(){
    menuClass();
});

menuClass();
em2px();
