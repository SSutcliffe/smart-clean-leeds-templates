<?php
require_once __DIR__ . '/monolith/monolith.php';


/**
 * Embed SVG
 *
 * Makes use of the SVG embedding function from Monolith with some added
 * guessing of file extensions and directories.
 *
 * @param string $file
 * @param boolean $no_frills
 * @return string
 */
function smart_embed_svg($file, $no_frills = false)
{
    // No extension?
    if (!\Cgit\Monolith\Core\endsWith($file, '.svg')) {
        $file .= '.svg';
    }

    // Local path?
    if (!\Cgit\Monolith\Core\startsWith($file, '/')) {
        $file = $_SERVER['DOCUMENT_ROOT'] . '/static/images/' . $file;
    }

    return \Cgit\Monolith\Core\embedSvg($file);
}
