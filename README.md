# Template template

A template for static templates. Note that `package.json` uses `*` for dependency versions, which will download the latest available versions. You may want to switch to something like `^2.1.x` to ensure you only get versions compatible with, for example, 2.1.x. See [documentation](https://docs.npmjs.com/files/package.json#dependencies).
