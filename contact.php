<!DOCTYPE html>

<html lang="en">

<head>

<meta charset="utf-8" />

<title>Templates</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="stylesheet" href="/css/style.css" />

<?php include_once("/functions.php"); ?>

</head>

<body>
<div class="size"></div>
<div class="nav-overlay"></div>
<div class="header">
    <div class="wrapper">
        <h1 class="title">
            <a href="">
                <img src="/static/images/logo.png">
            </a>
        </h1>
        <div class="phone-tag-wrapper">
            <a href="" class="header-phone">0113 1234 5678</a>
            <p class="header-tag-line">"The Complete Cleaning Solution"</p>
        </div>
        <div class="nav-toggle"></div>
        <div class="nav">
            <ul>
                <li><a href="/index.php">Home</a></li>
                <li><a href="/inner.php">About</a></li>
                <li class="current-menu-item"><a class="child-pages" href="">Services</a></li>
                <li><a href="">News</a></li>
                <li><a href="/contact.php">Contact</a></li>
            </ul>
        </div>
        <a href="" class="header-get-quote">Get a quote</a>
    </div>
</div>
<div class="main-content-container">
    <div class="inner-banner">
        <div class="wrapper">
            <div class="inner-wrapper">
                <h1>Commerical carpet cleaning</h1>
                <div class="inner-accreditations">
                    <div class="accrediation"></div>
                    <div class="accrediation"></div>
                    <div class="accrediation"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="inner-bread-crumb">
        <div class="wrapper">
            <div class="inner-wrapper">
                <a href="" class="cgit-breadcrumb-item link">Home</a>
                <a href="" class="cgit-breadcrumb-item link">About</a>
                <span class="cgit-breadcrumb-item span">Team</span>
            </div>
        </div>
    </div>
    <div class="inner-main-content">
        <div class="wrapper">
            <div class="inner-wrapper">
                <div class="inner-main">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pellentesque est sit amet nunc</h1>
                    <h3>Interdum et malesuada fames</h3>
                    <form>
                        <div class="field-container">
                            <div class="field">
                                <label class="screen-reader-text"  for="fullname">Name</label>
                                <input type="contact_form_name" name="fullname" placeholder="Name" value="" required />
                            </div>
                            <div class="field">
                                <label class="screen-reader-text" for=email"">Email</label>
                                <input type="contact_form_email" name="email" placeholder="Email" value="" required />
                            </div>
                            <div class="field">
                                <label class="screen-reader-text" for="tel">Phone Number</label>
                                <input type="contact_form_tel" name="tel" placeholder="Phone Number" value="" required />
                            </div>

                        <h2>Service(s) interested in*</h2>

                            <div class="check">
                                <input class="checkbox" type="checkbox" name="type" id="contact_form_domestic_carpet_cleaning" value="Domestic Carpet Cleaning"  />
                                <label for="contact_form_domestic_carpet_cleaning">Domestic Carpet Cleaning</label>
                            </div>
                            <div class="check">
                                <input class="checkbox" type="checkbox" name="type" id="contact_form_commercial_cleaning" value="Commercial Cleaning"  />
                                <label for="contact_form_commercial_cleaning">Commercial Cleaning</label>
                            </div>
                            <div class="check">
                                <input class="checkbox" type="checkbox" name="type" id="contact_form_end_tenancy" value="End of tenancy cleaning"  />
                                <label for="contact_form_end_tenancy">End of tenancy cleaning</label>
                            </div>
                            <div class="check">
                                <input class="checkbox" type="checkbox" name="type" id="contact_form_other_enquiry" value="Other Enquiry"  />
                                <label for="contact_form_other_enquiry">Other</label>
                            </div>
                        </div>
                        <div class="field">
                            <textarea id="contact_form_message" name="message" placeholder="How can we help?" class="text-input area" rows="14" required></textarea>
                            <button type="submit" class="send-button">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="inner-side">
                    <div class="side-get-quote">
                        <h3 class="get-quote-heading">Meet the team</h3>
                        <p class="get-quote-text">We can provide you with a free no obligation quote for any of our cleaning services</p>
                        <a class="get-quote-link" href="/contact/">Get in touch</a>
                    </div>
                    <div class="side-orange-nav">
                        <a class="orange-nav-button" href="">Book an appointment</a>
                        <a class="orange-nav-button" href="">Commercial services</a>
                        <a class="orange-nav-button" href="">Domestic services</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonal-container">
        <div class="wrapper">
                <h1 class="heading">Customer feedback</h1>
            <a class="prev" href="#"></a>
            <div class="carousel">
                <p class="testimonal">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin et elit imperdiet, tincidunt urna vitae, laoreet tellus. Pellentesque sed risus viverra, imperdiet nisi et, tincidunt lacus. Vestibulum mollis erat ac tortor luctus euismod</p>
                <p class="testimonal">Proin et elit imperdiet, tincidunt urna vitae, laoreet tellus. Pellentesque sed risus viverra, imperdiet nisi et, tincidunt lacus. Vestibulum mollis erat ac tortor luctus euismod</p>
            </div>
            <a class="next" href="#"></a>
            <div class="trustist">
                <div ts-widget="" ts-review-link-font-family="Montserrat, sans-serif" ts-review-link-colour="#ffffff" ts-border-radius="10px"></div>
            </div>
        </div>
    </div>
    <div class="accrediation-container">
        <div class="wrapper">
            <div class="inner-wrapper">
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/bicsc.png"></a>
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/NCCA-Logo.png"></a>
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/bicsc.png"></a>
            </div>
        </div>
    </div>
    <div class="newsletter-container">
        <div class="wrapper">
            <div class="inner-wrapper">
                <label class="heading" for="newsletter-footer-email">Newsletter</label>
                <input id="newsletter-footer-email" type="text" name="newsletter-footer" class="text-input" />
                <button class="subscribe">Subscribe</button>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="wrapper">
        <div class="inner-wrapper">
            <div class="leeds-wrapper">
                <h1 class="heading">Smart Cleaning <span class="heading-span">Leeds</span></h1>
                <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Don ferme, magna a sagittis luctus, tellus orci vulputate diam, vel tincidunt felis libero id justo.</p>
                <div class="address">
                    <h3 class="heading">Address</h3>
                    <p class="content">Unit 13 Fairway Drive<br />
                    Upper Poppleton<br />
                    York<br />
                    YO26 6HE</p>
                </div>
                <div class="contact">
                    <h3 class="heading">Contact details</h3>
                    <a class="link" href="mailto:info@smartcleaninguk.co.uk">info@smartcleaninguk.co.uk</a><br />
                    <a class="link" href="tel:011312345678">0113 1234 5678</a>
                </div>
            </div>
            <div class="york-wrapper">
                <h1 class="heading">Smart Cleaning <span class="heading-span">York</span></h1>
                <p class="content">Aenean nec eros tempor, mattis nisl at, pharetra risus. Vivs fringilla pretium feugiat. Nam viverra nunc ac libero ornare, ac tempor elit auctor. Nulla quis velit eros.</p>
                <div class="contact">
                    <h3 class="heading">Contact details</h3>
                    <a class="link" href="mailto:info@smartcleaninguk.co.uk">info@smartcleaninguk.co.uk</a><br />
                    <a class="link" href="tel:011312345678">0113 1234 5678</a>
                </div>
                <div class="website">
                    <h3 class="heading">Website</h3>
                    <a class="link" href="http://www.smartcleaninguk.co.uk">www.smartcleaninguk.co.uk</a>
                </div>
            </div>
            <div class="social-wrapper">
                <a class="link" href=""><?= smart_embed_svg('facebook-box') ?></a>
                <a class="link" href=""><?= smart_embed_svg('twitter-box') ?></a>
                <a class="link" href=""><?= smart_embed_svg('linkedin-box') ?></a>
            </div>
            <div class="bottom-wrapper">
                <p class="content">© Smart Cleaning Group. Company Registration 09804292 VAT 257 353 393.<br />
                All rights reserved. <a class="link" href="">Disclaimer.</a><a class="link" href="">Privacy Policy</a><br />
                Site designed and built by <a class="link" href="">Castlegate IT in York</a></p>
                <div class="trustist">
                    <div ts-widget="" ts-review-link-font-family="Montserrat, sans-serif" ts-review-link-colour="#ffffff" ts-border-radius="10px"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/js/script.js"></script>
<script src="//widget.trustist.com/trustistreviewer?b=a6c35fey&l="></script>

</body>

</html>
