<!DOCTYPE html>

<html lang="en">

<head>

<meta charset="utf-8" />

<title>Templates</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="stylesheet" href="/css/style.css" />

<?php include_once("/functions.php"); ?>

</head>

<body>
<div class="size"></div>
<div class="nav-overlay"></div>
<div class="header">
    <div class="wrapper">
        <h1 class="title">
            <a href="">
                <img src="/static/images/logo.png">
            </a>
        </h1>
        <div class="phone-tag-wrapper">
            <a href="" class="header-phone">0113 1234 5678</a>
            <p class="header-tag-line">"The Complete Cleaning Solution"</p>
        </div>
        <div class="nav-toggle"></div>
        <div class="nav">
            <ul>
                <li><a href="/index.php">Home</a></li>
                <li><a href="/inner.php">About</a></li>
                <li class="current-menu-item"><a class="child-pages" href="">Services</a></li>
                <li><a href="">News</a></li>
                <li><a href="/contact.php">Contact</a></li>
            </ul>
        </div>
        <a href="" class="header-get-quote">Get a quote</a>
    </div>
</div>
<div class="main-content-container">
    <div class="inner-banner">
        <div class="wrapper">
            <div class="inner-wrapper">
                <h1>Commerical carpet cleaning</h1>
                <div class="inner-accreditations">
                    <div class="accrediation"></div>
                    <div class="accrediation"></div>
                    <div class="accrediation"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="inner-bread-crumb">
        <div class="wrapper">
            <div class="inner-wrapper">
                <a href="" class="cgit-breadcrumb-item link">Home</a>
                <a href="" class="cgit-breadcrumb-item link">About</a>
                <span class="cgit-breadcrumb-item span">Team</span>
            </div>
        </div>
    </div>
    <div class="inner-main-content">
        <div class="wrapper">
            <div class="inner-wrapper">
                <div class="inner-main">
                    <h1>Benefits of clean carpets in a commercial environment</h1>

                    <h3>Putting a spring back in your step.</h3>

                    <p>We are proud to offer meticulous commercial carpet cleaning in York and across the UK. Whether it being a bar, pub, restaurant, cinema, casino, bingo hall, office or educational facility we have the expertise to ensure your carpets are correctly cleaned.</p>

                    <p>After heavy usage, carpet fibres become soiled meaning not even hovering can fix the problem. Our professional and affordable York Carpet Cleaning Service will use a selection of tailored techniques to ensure carpets are returned to pristine condition with no shrinkage and fast drying time.</p>

                    <div class="accordion-section">
                        <a class="accordion">
                            <h3 class="accordion-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id?</h3>
                            <p class="accoridion-content">Nulla maximus purus tortor, eget cursus felis ultricies eu. Sed quis nibh maximus, eleifend massa quis, ullamcorper lacus. Maecenas turpis orci, iaculis vitae justo eget, maximus imperdiet sapien. Aliquam erat volutpat. Nunc sodales arcu leo, et tristique enim sodales vel. Nam ut tempus felis. Nullam in lectus sollicitudin, euismod odio non, luctus lorem.</p>
                        </a>
                        <a class="accordion">
                            <h3 class="accordion-heading">Quisque placerat fringilla nisi et ullamcorper. Cras in aliquam ante?</h3>
                            <p class="accoridion-content">Nulla maximus purus tortor, eget cursus felis ultricies eu. Sed quis nibh maximus, eleifend massa quis, ullamcorper lacus. Maecenas turpis orci, iaculis vitae justo eget, maximus imperdiet sapien. Aliquam erat volutpat. Nunc sodales arcu leo, et tristique enim sodales vel. Nam ut tempus felis. Nullam in lectus sollicitudin, euismod odio non, luctus lorem.</p>
                        </a>

                        <a class="accordion">
                            <h3 class="accordion-heading">Curabitur lectus purus, scelerisque id efficitur in?</h3>
                            <p class="accoridion-content">Nulla maximus purus tortor, eget cursus felis ultricies eu. Sed quis nibh maximus, eleifend massa quis, ullamcorper lacus. Maecenas turpis orci, iaculis vitae justo eget, maximus imperdiet sapien. Aliquam erat volutpat. Nunc sodales arcu leo, et tristique enim sodales vel. Nam ut tempus felis. Nullam in lectus sollicitudin, euismod odio non, luctus lorem.</p>
                        </a>
                        <a class="accordion">
                            <h3 class="accordion-heading">Fusce at elit gravida, interdum dolor in, eleifend dolor?</h3>
                        </a>
                        <a class="accordion">
                            <h3 class="accordion-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id?</h3>
                        </a>
                    </div>

                    <div class="gallery-section">
                        <div class="gallery-holder">
                            <img class="gallery-slide" src="http://via.placeholder.com/800x500">
                            <img class="gallery-slide" src="http://via.placeholder.com/800x500">
                        </div>
                        <div class="gallery-pager">

                        </div>
                    </div>

                    <h3>Quisque placerat fringilla nisi et ullamcorper. Cras in aliquam ante, eu facilisis enim. Curabitur lectus purus, scelerisque id efficitur in, sollicitudin id lacus. Morbi ut quam iaculis, finibus nisi vitae, pretium velit. </h3>

                    <p>Fusce at elit gravida, interdum dolor in, eleifend dolor. Morbi dictum molestie libero, quis semper mi. Nam efficitur at felis ut sagittis. In feugiat dui ac diam varius posuere. Ut elementum justo ut diam mollis, a gravida lorem porttitor. Mauris elementum dapibus tellus in scelerisque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</p>

                    <ul>
                        <li>Cras in aliquam ante</li>
                        <li>Curabitur lectus purus, scelerisque id efficitur</li>
                        <li>Morbi dictum molestie libero</li>
                        <li>Class aptent taciti sociosqu</li>
                    </ul>

                    <div class="table-section">
                        <table>
                            <tr>
                                <td>Lorem ipsum dolor sit amet</td>
                                <td>£100.00</td>
                            </tr>
                            <tr>
                                <td>Maecenas ullamcorper ante a sem elementum</td>
                                <td>£82.00</td>
                            </tr>
                            <tr>
                                <td>Maecenas ac lacinia mauris</td>
                                <td>£18.00</td>
                            </tr>
                            <tr>
                                <td>Suspendisse sollicitudin vitae lacus</td>
                                <td>£7.99</td>
                            </tr>
                            <tr>
                                <td>Nunc eget consectetur nisi</td>
                                <td>£10.00</td>
                            </tr>
                            <tr>
                                <td>Nunc maximus malesuada libero</td>
                                <td>£54.50</td>
                            </tr>
                            <tr>
                                <td>Duis eu blandit arcu</td>
                                <td>£280.00</td>
                            </tr>
                            <tr>
                                <td>Morbi ullamcorper, est quis congue sodales</td>
                                <td>£12.65</td>
                            </tr>
                        </table>
                    </div>
                    <a class="back-button" href="">Back</a>
                </div>
                <div class="inner-side">
                    <div class="side-sub-nav">
                        <h3>Commercial Carpet Cleaning</h3>
                        <ul>
                            <li><a href="">Subnav item</a></li>
                            <li><a href="">Subnav item</a></li>
                            <li><a href="">Subnav item</a></li>
                            <li><a href="">Subnav item</a></li>
                            <li><a href="">Subnav item</a></li>
                        </ul>
                    </div>
                    <div class="side-find-more">
                        <div class="find-more-container">
                            <div class="find-more-image -carpet"></div>
                            <h3 class="find-more-heading">Carpet Cleaning</h3>
                            <a class="find-more-link" href="/service/">Find out more</a>
                        </div>
                        <div class="find-more-container -commercial">
                            <div class="find-more-image"></div>
                            <h3 class="find-more-heading">Commercial Office Carpet Cleaning</h3>
                            <a class="find-more-link" href="/service/">Find out more</a>
                        </div>
                    </div>
                    <div class="side-get-quote">
                        <h3 class="get-quote-heading">GET A QUOTE</h3>
                        <p class="get-quote-text">We can provide you with a free no obligation quote for any of our cleaning services</p>
                        <a class="get-quote-link" href="/contact/">Get in touch</a>
                    </div>
                    <div class="side-orange-nav">
                        <a class="orange-nav-button" href="">Book an appointment</a>
                        <a class="orange-nav-button" href="">Commercial services</a>
                        <a class="orange-nav-button" href="">Domestic services</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonal-container">
        <div class="wrapper">
                <h1 class="heading">Customer feedback</h1>
            <a class="prev" href="#"></a>
            <div class="carousel">
                <p class="testimonal">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin et elit imperdiet, tincidunt urna vitae, laoreet tellus. Pellentesque sed risus viverra, imperdiet nisi et, tincidunt lacus. Vestibulum mollis erat ac tortor luctus euismod</p>
                <p class="testimonal">Proin et elit imperdiet, tincidunt urna vitae, laoreet tellus. Pellentesque sed risus viverra, imperdiet nisi et, tincidunt lacus. Vestibulum mollis erat ac tortor luctus euismod</p>
            </div>
            <a class="next" href="#"></a>
            <div class="trustist">
                <div ts-widget="" ts-review-link-font-family="Montserrat, sans-serif" ts-review-link-colour="#ffffff" ts-border-radius="10px"></div>
            </div>
        </div>
    </div>
    <div class="accrediation-container">
        <div class="wrapper">
            <div class="inner-wrapper">
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/bicsc.png"></a>
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/NCCA-Logo.png"></a>
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/bicsc.png"></a>
            </div>
        </div>
    </div>
    <div class="newsletter-container">
        <div class="wrapper">
            <div class="inner-wrapper">
                <label class="heading" for="newsletter-footer-email">Newsletter</label>
                <input id="newsletter-footer-email" type="text" name="newsletter-footer" class="text-input" />
                <button class="subscribe">Subscribe</button>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="wrapper">
        <div class="inner-wrapper">
            <div class="leeds-wrapper">
                <h1 class="heading">Smart Cleaning <span class="heading-span">Leeds</span></h1>
                <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Don ferme, magna a sagittis luctus, tellus orci vulputate diam, vel tincidunt felis libero id justo.</p>
                <div class="address">
                    <h3 class="heading">Address</h3>
                    <p class="content">Unit 13 Fairway Drive<br />
                    Upper Poppleton<br />
                    York<br />
                    YO26 6HE</p>
                </div>
                <div class="contact">
                    <h3 class="heading">Contact details</h3>
                    <a class="link" href="mailto:info@smartcleaninguk.co.uk">info@smartcleaninguk.co.uk</a><br />
                    <a class="link" href="tel:011312345678">0113 1234 5678</a>
                </div>
            </div>
            <div class="york-wrapper">
                <h1 class="heading">Smart Cleaning <span class="heading-span">York</span></h1>
                <p class="content">Aenean nec eros tempor, mattis nisl at, pharetra risus. Vivs fringilla pretium feugiat. Nam viverra nunc ac libero ornare, ac tempor elit auctor. Nulla quis velit eros.</p>
                <div class="contact">
                    <h3 class="heading">Contact details</h3>
                    <a class="link" href="mailto:info@smartcleaninguk.co.uk">info@smartcleaninguk.co.uk</a><br />
                    <a class="link" href="tel:011312345678">0113 1234 5678</a>
                </div>
                <div class="website">
                    <h3 class="heading">Website</h3>
                    <a class="link" href="http://www.smartcleaninguk.co.uk">www.smartcleaninguk.co.uk</a>
                </div>
            </div>
            <div class="social-wrapper">
                <a class="link" href=""><?= smart_embed_svg('facebook-box') ?></a>
                <a class="link" href=""><?= smart_embed_svg('twitter-box') ?></a>
                <a class="link" href=""><?= smart_embed_svg('linkedin-box') ?></a>
            </div>
            <div class="bottom-wrapper">
                <p class="content">© Smart Cleaning Group. Company Registration 09804292 VAT 257 353 393.<br />
                All rights reserved. <a class="link" href="">Disclaimer.</a><a class="link" href="">Privacy Policy</a><br />
                Site designed and built by <a class="link" href="">Castlegate IT in York</a></p>
                <div class="trustist">
                    <div ts-widget="" ts-review-link-font-family="Montserrat, sans-serif" ts-review-link-colour="#ffffff" ts-border-radius="10px"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/js/script.js"></script>
<script src="//widget.trustist.com/trustistreviewer?b=a6c35fey&l="></script>

</body>

</html>
