<!DOCTYPE html>

<html lang="en">

<head>

<meta charset="utf-8" />

<title>Templates</title>

<meta name="viewport" content="width=device-width, initial-scale=1" />

<link rel="stylesheet" href="/css/style.css" />

<?php include_once("/functions.php"); ?>

</head>

<body>
<div class="size"></div>
<div class="nav-overlay"></div>
<div id="header" class="header">
    <div class="wrapper">
        <h1 class="title">
            <a href="">
                <img src="/static/images/logo.png" alt="">
            </a>
        </h1>
        <div class="phone-tag-wrapper">
            <a href="" class="header-phone">0113 1234 5678</a>
            <p class="header-tag-line">"The Complete Cleaning Solution"</p>
        </div>
        <div class="nav-toggle"></div>
        <div class="nav">
            <ul>
                <li><a href="/index.php">Home</a></li>
                <li><a href="/inner.php">About</a></li>
                <li class="current-menu-item"><a class="child-pages" href="">Services</a></li>
                <li><a href="">News</a></li>
                <li><a href="/contact.php">Contact</a></li>
            </ul>
        </div>
        <a href="" class="header-get-quote">Get a quote</a>
    </div>
</div>
<div class="main-content-container">
    <div class="home-banner">
        <div class="wrapper">
            <div class="inner-wrapper">
                <div class="left-wrapper">
                    <h1>Lorem ipsum dolor sit</h1>
                    <h2>Consectetur adipiscing elit. Vivamus semper magna id felis viverra pharetra eu vitae augue. Nullam tincidunt lacus ullamcorper metus dignissim feugiat.</h2>
                    <a class="find-out-button" href="">Find out more</a>
                    <a class="book-app-button" href="">Book appointment</a>
                </div>
                <div class="right-wrapper">
                    <div class="circle-banner-image"></div>
                    <div class="inner-accreditations">
                        <div class="accrediation"></div>
                        <div class="accrediation"></div>
                        <div class="accrediation"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content">
        <div class="wrapper">
            <div class="inner-wrapper">
                <div class="home-main">
                    <div class="home-services">
                        <div class="service">
                            <div class="image"></div>
                            <h3 class="heading">Carpet Cleaning</h3>
                            <p class="description">Get your carpets looking like new again! With the latest carpet cleaning techniques and knowledge.</p>
                            <a href="" class="button">Find out more</a>
                        </div>
                        <div class="service">
                            <div class="image"></div>
                            <h3 class="heading">Carpet Cleaning</h3>
                            <p class="description">Get your carpets looking like new again! With the latest carpet cleaning techniques and knowledge.</p>
                            <a href="" class="button">Find out more</a>
                        </div>
                        <div class="service">
                            <div class="image"></div>
                            <h3 class="heading">Carpet Cleaning</h3>
                            <p class="description">Get your carpets looking like new again! With the latest carpet cleaning techniques and knowledge.</p>
                            <a href="" class="button">Find out more</a>
                        </div>
                        <div class="service">
                            <div class="image"></div>
                            <h3 class="heading">Carpet Cleaning</h3>
                            <p class="description">Get your carpets looking like new again! With the latest carpet cleaning techniques and knowledge.</p>
                            <a href="" class="button">Find out more</a>
                        </div>
                    </div>
                    <div class="home-text-image-container">
                        <div class="wrapper-text-left">
                            <div class="left-column">
                                <h1 class="heading">Why Choose Smart Cleaning Carpet Cleaning Leeds?</h1>
                                <p class="content">Professionally trained and accredited by the National Carpet Cleaning Association (NCCA), we don’t dabble, we specialise in Carpet & Upholstery Cleaning. For your peace of mind we carry a fully comprehensive Insurance which we are a covered up to £5 million. So that you know who actually is coming to your home, check out our ABOUT page to put a face to the service. We simply don’t just employ anyone, we thoroughly vet our operatives and they each have undergone a DBS check (formerly CRB) to ensure extra peace of mind for you and us.</p>
                                <p class="content -bold">For a professional service which you can rely on, accredited by a leading body, contact Smart Cleaning today for a FREE no obligation quotation or for a friendly chat and advice.</p>
                                <a class="find-out-button" href="">Find out more</a>
                                <a class="book-app-button" href="">Book appointment</a>
                            </div>
                            <div class="right-column">
                                <div class="gallery-section">
                                    <div class="gallery-holder">
                                        <img class="gallery-slide" src="/static/images/banner-about.png">
                                        <img class="gallery-slide" src="/static/images/banner-domestic-carpet.jpg">
                                    </div>
                                    <div class="gallery-pager"></div>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper-text-right">
                            <div class="left-column">
                                <img src="http://via.placeholder.com/800x500">
                            </div>
                            <div class="right-column">
                                <h1 class="heading">The cleaning services in Leeds you can trust</h1>
                                <ul>
                                    <li>Complete range of commercial cleaning services</li>
                                    <li>Affordable price, so you get value for money</li>
                                    <li>Comprehensive insurance</li>
                                    <li>All of our staff are DBS checked</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonal-container">
        <div class="wrapper">
                <h1 class="heading">Customer feedback</h1>
            <a class="prev" href="#"></a>
            <div class="carousel">
                <p class="testimonal">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin et elit imperdiet, tincidunt urna vitae, laoreet tellus. Pellentesque sed risus viverra, imperdiet nisi et, tincidunt lacus. Vestibulum mollis erat ac tortor luctus euismod</p>
                <p class="testimonal">Proin et elit imperdiet, tincidunt urna vitae, laoreet tellus. Pellentesque sed risus viverra, imperdiet nisi et, tincidunt lacus. Vestibulum mollis erat ac tortor luctus euismod</p>
            </div>
            <a class="next" href="#"></a>
            <div class="trustist">
                <div ts-widget="" ts-review-link-font-family="Montserrat, sans-serif" ts-review-link-colour="#ffffff" ts-border-radius="10px"></div>
            </div>
        </div>
    </div>
    <div class="accrediation-container">
        <div class="wrapper">
            <div class="inner-wrapper">
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/bicsc.png"></a>
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/ncca-logo.png"></a>
                <a class="accrediation" href=""><img class="acc-image" src="/static/images/bicsc.png"></a>
            </div>
        </div>
    </div>
    <div class="newsletter-container">
        <div class="wrapper">
            <div class="inner-wrapper">
                <label class="heading" for="newsletter-footer-email">Newsletter</label>
                <input id="newsletter-footer-email" type="text" name="newsletter-footer" class="text-input" />
                <button class="subscribe">Subscribe</button>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="wrapper">
        <div class="inner-wrapper">
            <div class="leeds-wrapper">
                <h1 class="heading">Smart Cleaning <span class="heading-span">Leeds</span></h1>
                <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Don ferme, magna a sagittis luctus, tellus orci vulputate diam, vel tincidunt felis libero id justo.</p>
                <div class="address">
                    <h3 class="heading">Address</h3>
                    <p class="content">Unit 13 Fairway Drive<br />
                    Upper Poppleton<br />
                    York<br />
                    YO26 6HE</p>
                </div>
                <div class="contact">
                    <h3 class="heading">Contact details</h3>
                    <a class="link" href="mailto:info@smartcleaninguk.co.uk">info@smartcleaninguk.co.uk</a><br />
                    <a class="link" href="tel:011312345678">0113 1234 5678</a>
                </div>
            </div>
            <div class="york-wrapper">
                <h1 class="heading">Smart Cleaning <span class="heading-span">York</span></h1>
                <p class="content">Aenean nec eros tempor, mattis nisl at, pharetra risus. Vivs fringilla pretium feugiat. Nam viverra nunc ac libero ornare, ac tempor elit auctor. Nulla quis velit eros.</p>
                <div class="contact">
                    <h3 class="heading">Contact details</h3>
                    <a class="link" href="mailto:info@smartcleaninguk.co.uk">info@smartcleaninguk.co.uk</a><br />
                    <a class="link" href="tel:011312345678">0113 1234 5678</a>
                </div>
                <div class="website">
                    <h3 class="heading">Website</h3>
                    <a class="link" href="http://www.smartcleaninguk.co.uk">www.smartcleaninguk.co.uk</a>
                </div>
            </div>
            <div class="social-wrapper">
                <a class="link" href=""><?= smart_embed_svg('facebook-box') ?></a>
                <a class="link" href=""><?= smart_embed_svg('twitter-box') ?></a>
                <a class="link" href=""><?= smart_embed_svg('linkedin-box') ?></a>
            </div>
            <div class="bottom-wrapper">
                <p class="content">© Smart Cleaning Group. Company Registration 09804292 VAT 257 353 393.<br />
                All rights reserved. <a class="link" href="">Disclaimer.</a><a class="link" href="">Privacy Policy</a><br />
                Site designed and built by <a class="link" href="">Castlegate IT in York</a></p>
                <div class="trustist">
                    <div ts-widget="" ts-review-link-font-family="Montserrat, sans-serif" ts-review-link-colour="#ffffff" ts-border-radius="10px"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/js/script.js"></script>
<script src="//widget.trustist.com/trustistreviewer?b=a6c35fey&l="></script>

</body>

</html>
