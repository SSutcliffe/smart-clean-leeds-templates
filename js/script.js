!function(e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e);
    } : t(e);
}("undefined" != typeof window ? window : this, function(A, e) {
    "use strict";
    var t = [], T = A.document, i = Object.getPrototypeOf, a = t.slice, g = t.concat, l = t.push, r = t.indexOf, n = {}, o = n.toString, v = n.hasOwnProperty, s = v.toString, c = s.call(Object), y = {}, m = function e(t) {
        return "function" == typeof t && "number" != typeof t.nodeType;
    }, x = function e(t) {
        return null != t && t === t.window;
    }, u = {
        type: !0,
        src: !0,
        noModule: !0
    };
    function b(e, t, n) {
        var i, r = (t = t || T).createElement("script");
        if (r.text = e, n) for (i in u) n[i] && (r[i] = n[i]);
        t.head.appendChild(r).parentNode.removeChild(r);
    }
    function w(e) {
        return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? n[o.call(e)] || "object" : typeof e;
    }
    var d = "3.3.1", C = function(e, t) {
        return new C.fn.init(e, t);
    }, f = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    C.fn = C.prototype = {
        jquery: "3.3.1",
        constructor: C,
        length: 0,
        toArray: function() {
            return a.call(this);
        },
        get: function(e) {
            return null == e ? a.call(this) : e < 0 ? this[e + this.length] : this[e];
        },
        pushStack: function(e) {
            var t = C.merge(this.constructor(), e);
            return t.prevObject = this, t;
        },
        each: function(e) {
            return C.each(this, e);
        },
        map: function(n) {
            return this.pushStack(C.map(this, function(e, t) {
                return n.call(e, t, e);
            }));
        },
        slice: function() {
            return this.pushStack(a.apply(this, arguments));
        },
        first: function() {
            return this.eq(0);
        },
        last: function() {
            return this.eq(-1);
        },
        eq: function(e) {
            var t = this.length, n = +e + (e < 0 ? t : 0);
            return this.pushStack(n >= 0 && n < t ? [ this[n] ] : []);
        },
        end: function() {
            return this.prevObject || this.constructor();
        },
        push: l,
        sort: t.sort,
        splice: t.splice
    }, C.extend = C.fn.extend = function() {
        var e, t, n, i, r, o, s = arguments[0] || {}, a = 1, l = arguments.length, c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || m(s) || (s = {}), 
        a === l && (s = this, a--); a < l; a++) if (null != (e = arguments[a])) for (t in e) n = s[t], 
        s !== (i = e[t]) && (c && i && (C.isPlainObject(i) || (r = Array.isArray(i))) ? (r ? (r = !1, 
        o = n && Array.isArray(n) ? n : []) : o = n && C.isPlainObject(n) ? n : {}, s[t] = C.extend(c, o, i)) : void 0 !== i && (s[t] = i));
        return s;
    }, C.extend({
        expando: "jQuery" + ("3.3.1" + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(e) {
            throw new Error(e);
        },
        noop: function() {},
        isPlainObject: function(e) {
            var t, n;
            return !(!e || "[object Object]" !== o.call(e)) && (!(t = i(e)) || "function" == typeof (n = v.call(t, "constructor") && t.constructor) && s.call(n) === c);
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e) return !1;
            return !0;
        },
        globalEval: function(e) {
            b(e);
        },
        each: function(e, t) {
            var n, i = 0;
            if (p(e)) {
                for (n = e.length; i < n; i++) if (!1 === t.call(e[i], i, e[i])) break;
            } else for (i in e) if (!1 === t.call(e[i], i, e[i])) break;
            return e;
        },
        trim: function(e) {
            return null == e ? "" : (e + "").replace(f, "");
        },
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (p(Object(e)) ? C.merge(n, "string" == typeof e ? [ e ] : e) : l.call(n, e)), 
            n;
        },
        inArray: function(e, t, n) {
            return null == t ? -1 : r.call(t, e, n);
        },
        merge: function(e, t) {
            for (var n = +t.length, i = 0, r = e.length; i < n; i++) e[r++] = t[i];
            return e.length = r, e;
        },
        grep: function(e, t, n) {
            for (var i, r = [], o = 0, s = e.length, a = !n; o < s; o++) (i = !t(e[o], o)) !== a && r.push(e[o]);
            return r;
        },
        map: function(e, t, n) {
            var i, r, o = 0, s = [];
            if (p(e)) for (i = e.length; o < i; o++) null != (r = t(e[o], o, n)) && s.push(r); else for (o in e) null != (r = t(e[o], o, n)) && s.push(r);
            return g.apply([], s);
        },
        guid: 1,
        support: y
    }), "function" == typeof Symbol && (C.fn[Symbol.iterator] = t[Symbol.iterator]), 
    C.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(e, t) {
        n["[object " + t + "]"] = t.toLowerCase();
    });
    function p(e) {
        var t = !!e && "length" in e && e.length, n = w(e);
        return !m(e) && !x(e) && ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e);
    }
    var h = function(n) {
        var e, p, b, o, r, h, d, g, w, l, c, S, A, s, T, v, a, u, y, C = "sizzle" + 1 * new Date(), m = n.document, I = 0, i = 0, f = se(), x = se(), P = se(), k = function(e, t) {
            return e === t && (c = !0), 0;
        }, E = {}.hasOwnProperty, t = [], D = t.pop, N = t.push, j = t.push, q = t.slice, H = function(e, t) {
            for (var n = 0, i = e.length; n < i; n++) if (e[n] === t) return n;
            return -1;
        }, O = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", L = "[\\x20\\t\\r\\n\\f]", _ = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+", R = "\\[" + L + "*(" + _ + ")(?:" + L + "*([*^$|!~]?=)" + L + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + _ + "))|)" + L + "*\\]", W = ":(" + _ + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + R + ")*)|.*)\\)|)", F = new RegExp(L + "+", "g"), M = new RegExp("^" + L + "+|((?:^|[^\\\\])(?:\\\\.)*)" + L + "+$", "g"), $ = new RegExp("^" + L + "*," + L + "*"), z = new RegExp("^" + L + "*([>+~]|" + L + ")" + L + "*"), B = new RegExp("=" + L + "*([^\\]'\"]*?)" + L + "*\\]", "g"), V = new RegExp(W), U = new RegExp("^" + _ + "$"), X = {
            ID: new RegExp("^#(" + _ + ")"),
            CLASS: new RegExp("^\\.(" + _ + ")"),
            TAG: new RegExp("^(" + _ + "|[*])"),
            ATTR: new RegExp("^" + R),
            PSEUDO: new RegExp("^" + W),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + L + "*(even|odd|(([+-]|)(\\d*)n|)" + L + "*(?:([+-]|)" + L + "*(\\d+)|))" + L + "*\\)|)", "i"),
            bool: new RegExp("^(?:" + O + ")$", "i"),
            needsContext: new RegExp("^" + L + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + L + "*((?:-\\d)?\\d*)" + L + "*\\)|)(?=[^-]|$)", "i")
        }, Q = /^(?:input|select|textarea|button)$/i, Z = /^h\d$/i, G = /^[^{]+\{\s*\[native \w/, Y = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, J = /[+~]/, K = new RegExp("\\\\([\\da-f]{1,6}" + L + "?|(" + L + ")|.)", "ig"), ee = function(e, t, n) {
            var i = "0x" + t - 65536;
            return i !== i || n ? t : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320);
        }, te = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, ne = function(e, t) {
            return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e;
        }, ie = function() {
            S();
        }, re = me(function(e) {
            return !0 === e.disabled && ("form" in e || "label" in e);
        }, {
            dir: "parentNode",
            next: "legend"
        });
        try {
            j.apply(t = q.call(m.childNodes), m.childNodes), t[m.childNodes.length].nodeType;
        } catch (n) {
            j = {
                apply: t.length ? function(e, t) {
                    N.apply(e, q.call(t));
                } : function(e, t) {
                    var n = e.length, i = 0;
                    while (e[n++] = t[i++]) ;
                    e.length = n - 1;
                }
            };
        }
        function oe(e, t, n, i) {
            var r, o, s, a, l, c, u, d = t && t.ownerDocument, f = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== f && 9 !== f && 11 !== f) return n;
            if (!i && ((t ? t.ownerDocument || t : m) !== A && S(t), t = t || A, T)) {
                if (11 !== f && (l = Y.exec(e))) if (r = l[1]) {
                    if (9 === f) {
                        if (!(s = t.getElementById(r))) return n;
                        if (s.id === r) return n.push(s), n;
                    } else if (d && (s = d.getElementById(r)) && y(t, s) && s.id === r) return n.push(s), 
                    n;
                } else {
                    if (l[2]) return j.apply(n, t.getElementsByTagName(e)), n;
                    if ((r = l[3]) && p.getElementsByClassName && t.getElementsByClassName) return j.apply(n, t.getElementsByClassName(r)), 
                    n;
                }
                if (p.qsa && !P[e + " "] && (!v || !v.test(e))) {
                    if (1 !== f) d = t, u = e; else if ("object" !== t.nodeName.toLowerCase()) {
                        (a = t.getAttribute("id")) ? a = a.replace(te, ne) : t.setAttribute("id", a = C), 
                        o = (c = h(e)).length;
                        while (o--) c[o] = "#" + a + " " + ye(c[o]);
                        u = c.join(","), d = J.test(e) && ge(t.parentNode) || t;
                    }
                    if (u) try {
                        return j.apply(n, d.querySelectorAll(u)), n;
                    } catch (e) {} finally {
                        a === C && t.removeAttribute("id");
                    }
                }
            }
            return g(e.replace(M, "$1"), t, n, i);
        }
        function se() {
            var n = [];
            function i(e, t) {
                return n.push(e + " ") > b.cacheLength && delete i[n.shift()], i[e + " "] = t;
            }
            return i;
        }
        function ae(e) {
            return e[C] = !0, e;
        }
        function le(e) {
            var t = A.createElement("fieldset");
            try {
                return !!e(t);
            } catch (e) {
                return !1;
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null;
            }
        }
        function ce(e, t) {
            var n = e.split("|"), i = n.length;
            while (i--) b.attrHandle[n[i]] = t;
        }
        function ue(e, t) {
            var n = t && e, i = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (i) return i;
            if (n) while (n = n.nextSibling) if (n === t) return -1;
            return e ? 1 : -1;
        }
        function de(t) {
            return function(e) {
                return "input" === e.nodeName.toLowerCase() && e.type === t;
            };
        }
        function fe(n) {
            return function(e) {
                var t = e.nodeName.toLowerCase();
                return ("input" === t || "button" === t) && e.type === n;
            };
        }
        function pe(t) {
            return function(e) {
                return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && re(e) === t : e.disabled === t : "label" in e && e.disabled === t;
            };
        }
        function he(s) {
            return ae(function(o) {
                return o = +o, ae(function(e, t) {
                    var n, i = s([], e.length, o), r = i.length;
                    while (r--) e[n = i[r]] && (e[n] = !(t[n] = e[n]));
                });
            });
        }
        function ge(e) {
            return e && "undefined" != typeof e.getElementsByTagName && e;
        }
        p = oe.support = {}, r = oe.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName;
        }, S = oe.setDocument = function(e) {
            var t, n, i = e ? e.ownerDocument || e : m;
            return i !== A && 9 === i.nodeType && i.documentElement ? (A = i, s = A.documentElement, 
            T = !r(A), m !== A && (n = A.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", ie, !1) : n.attachEvent && n.attachEvent("onunload", ie)), 
            p.attributes = le(function(e) {
                return e.className = "i", !e.getAttribute("className");
            }), p.getElementsByTagName = le(function(e) {
                return e.appendChild(A.createComment("")), !e.getElementsByTagName("*").length;
            }), p.getElementsByClassName = G.test(A.getElementsByClassName), p.getById = le(function(e) {
                return s.appendChild(e).id = C, !A.getElementsByName || !A.getElementsByName(C).length;
            }), p.getById ? (b.filter.ID = function(e) {
                var t = e.replace(K, ee);
                return function(e) {
                    return e.getAttribute("id") === t;
                };
            }, b.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && T) {
                    var n = t.getElementById(e);
                    return n ? [ n ] : [];
                }
            }) : (b.filter.ID = function(e) {
                var n = e.replace(K, ee);
                return function(e) {
                    var t = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
                    return t && t.value === n;
                };
            }, b.find.ID = function(e, t) {
                if ("undefined" != typeof t.getElementById && T) {
                    var n, i, r, o = t.getElementById(e);
                    if (o) {
                        if ((n = o.getAttributeNode("id")) && n.value === e) return [ o ];
                        r = t.getElementsByName(e), i = 0;
                        while (o = r[i++]) if ((n = o.getAttributeNode("id")) && n.value === e) return [ o ];
                    }
                    return [];
                }
            }), b.find.TAG = p.getElementsByTagName ? function(e, t) {
                return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : p.qsa ? t.querySelectorAll(e) : void 0;
            } : function(e, t) {
                var n, i = [], r = 0, o = t.getElementsByTagName(e);
                if ("*" === e) {
                    while (n = o[r++]) 1 === n.nodeType && i.push(n);
                    return i;
                }
                return o;
            }, b.find.CLASS = p.getElementsByClassName && function(e, t) {
                if ("undefined" != typeof t.getElementsByClassName && T) return t.getElementsByClassName(e);
            }, a = [], v = [], (p.qsa = G.test(A.querySelectorAll)) && (le(function(e) {
                s.appendChild(e).innerHTML = "<a id='" + C + "'></a><select id='" + C + "-\r\\' msallowcapture=''><option selected=''></option></select>", 
                e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + L + "*(?:''|\"\")"), 
                e.querySelectorAll("[selected]").length || v.push("\\[" + L + "*(?:value|" + O + ")"), 
                e.querySelectorAll("[id~=" + C + "-]").length || v.push("~="), e.querySelectorAll(":checked").length || v.push(":checked"), 
                e.querySelectorAll("a#" + C + "+*").length || v.push(".#.+[+~]");
            }), le(function(e) {
                e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var t = A.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + L + "*[*^$|!~]?="), 
                2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), 
                s.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), 
                e.querySelectorAll("*,:x"), v.push(",.*:");
            })), (p.matchesSelector = G.test(u = s.matches || s.webkitMatchesSelector || s.mozMatchesSelector || s.oMatchesSelector || s.msMatchesSelector)) && le(function(e) {
                p.disconnectedMatch = u.call(e, "*"), u.call(e, "[s!='']:x"), a.push("!=", W);
            }), v = v.length && new RegExp(v.join("|")), a = a.length && new RegExp(a.join("|")), 
            t = G.test(s.compareDocumentPosition), y = t || G.test(s.contains) ? function(e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e, i = t && t.parentNode;
                return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)));
            } : function(e, t) {
                if (t) while (t = t.parentNode) if (t === e) return !0;
                return !1;
            }, k = t ? function(e, t) {
                if (e === t) return c = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !p.sortDetached && t.compareDocumentPosition(e) === n ? e === A || e.ownerDocument === m && y(m, e) ? -1 : t === A || t.ownerDocument === m && y(m, t) ? 1 : l ? H(l, e) - H(l, t) : 0 : 4 & n ? -1 : 1);
            } : function(e, t) {
                if (e === t) return c = !0, 0;
                var n, i = 0, r = e.parentNode, o = t.parentNode, s = [ e ], a = [ t ];
                if (!r || !o) return e === A ? -1 : t === A ? 1 : r ? -1 : o ? 1 : l ? H(l, e) - H(l, t) : 0;
                if (r === o) return ue(e, t);
                n = e;
                while (n = n.parentNode) s.unshift(n);
                n = t;
                while (n = n.parentNode) a.unshift(n);
                while (s[i] === a[i]) i++;
                return i ? ue(s[i], a[i]) : s[i] === m ? -1 : a[i] === m ? 1 : 0;
            }, A) : A;
        }, oe.matches = function(e, t) {
            return oe(e, null, null, t);
        }, oe.matchesSelector = function(e, t) {
            if ((e.ownerDocument || e) !== A && S(e), t = t.replace(B, "='$1']"), p.matchesSelector && T && !P[t + " "] && (!a || !a.test(t)) && (!v || !v.test(t))) try {
                var n = u.call(e, t);
                if (n || p.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n;
            } catch (e) {}
            return oe(t, A, null, [ e ]).length > 0;
        }, oe.contains = function(e, t) {
            return (e.ownerDocument || e) !== A && S(e), y(e, t);
        }, oe.attr = function(e, t) {
            (e.ownerDocument || e) !== A && S(e);
            var n = b.attrHandle[t.toLowerCase()], i = n && E.call(b.attrHandle, t.toLowerCase()) ? n(e, t, !T) : void 0;
            return void 0 !== i ? i : p.attributes || !T ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null;
        }, oe.escape = function(e) {
            return (e + "").replace(te, ne);
        }, oe.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e);
        }, oe.uniqueSort = function(e) {
            var t, n = [], i = 0, r = 0;
            if (c = !p.detectDuplicates, l = !p.sortStable && e.slice(0), e.sort(k), c) {
                while (t = e[r++]) t === e[r] && (i = n.push(r));
                while (i--) e.splice(n[i], 1);
            }
            return l = null, e;
        }, o = oe.getText = function(e) {
            var t, n = "", i = 0, r = e.nodeType;
            if (r) {
                if (1 === r || 9 === r || 11 === r) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += o(e);
                } else if (3 === r || 4 === r) return e.nodeValue;
            } else while (t = e[i++]) n += o(t);
            return n;
        }, (b = oe.selectors = {
            cacheLength: 50,
            createPseudo: ae,
            match: X,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(K, ee), e[3] = (e[3] || e[4] || e[5] || "").replace(K, ee), 
                    "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4);
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || oe.error(e[0]), 
                    e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && oe.error(e[0]), 
                    e;
                },
                PSEUDO: function(e) {
                    var t, n = !e[6] && e[2];
                    return X.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && V.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), 
                    e[2] = n.slice(0, t)), e.slice(0, 3));
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(K, ee).toLowerCase();
                    return "*" === e ? function() {
                        return !0;
                    } : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t;
                    };
                },
                CLASS: function(e) {
                    var t = f[e + " "];
                    return t || (t = new RegExp("(^|" + L + ")" + e + "(" + L + "|$)")) && f(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "");
                    });
                },
                ATTR: function(n, i, r) {
                    return function(e) {
                        var t = oe.attr(e, n);
                        return null == t ? "!=" === i : !i || (t += "", "=" === i ? t === r : "!=" === i ? t !== r : "^=" === i ? r && 0 === t.indexOf(r) : "*=" === i ? r && t.indexOf(r) > -1 : "$=" === i ? r && t.slice(-r.length) === r : "~=" === i ? (" " + t.replace(F, " ") + " ").indexOf(r) > -1 : "|=" === i && (t === r || t.slice(0, r.length + 1) === r + "-"));
                    };
                },
                CHILD: function(h, e, t, g, v) {
                    var y = "nth" !== h.slice(0, 3), m = "last" !== h.slice(-4), x = "of-type" === e;
                    return 1 === g && 0 === v ? function(e) {
                        return !!e.parentNode;
                    } : function(e, t, n) {
                        var i, r, o, s, a, l, c = y !== m ? "nextSibling" : "previousSibling", u = e.parentNode, d = x && e.nodeName.toLowerCase(), f = !n && !x, p = !1;
                        if (u) {
                            if (y) {
                                while (c) {
                                    s = e;
                                    while (s = s[c]) if (x ? s.nodeName.toLowerCase() === d : 1 === s.nodeType) return !1;
                                    l = c = "only" === h && !l && "nextSibling";
                                }
                                return !0;
                            }
                            if (l = [ m ? u.firstChild : u.lastChild ], m && f) {
                                p = (a = (i = (r = (o = (s = u)[C] || (s[C] = {}))[s.uniqueID] || (o[s.uniqueID] = {}))[h] || [])[0] === I && i[1]) && i[2], 
                                s = a && u.childNodes[a];
                                while (s = ++a && s && s[c] || (p = a = 0) || l.pop()) if (1 === s.nodeType && ++p && s === e) {
                                    r[h] = [ I, a, p ];
                                    break;
                                }
                            } else if (f && (p = a = (i = (r = (o = (s = e)[C] || (s[C] = {}))[s.uniqueID] || (o[s.uniqueID] = {}))[h] || [])[0] === I && i[1]), 
                            !1 === p) while (s = ++a && s && s[c] || (p = a = 0) || l.pop()) if ((x ? s.nodeName.toLowerCase() === d : 1 === s.nodeType) && ++p && (f && ((r = (o = s[C] || (s[C] = {}))[s.uniqueID] || (o[s.uniqueID] = {}))[h] = [ I, p ]), 
                            s === e)) break;
                            return (p -= v) === g || p % g == 0 && p / g >= 0;
                        }
                    };
                },
                PSEUDO: function(e, o) {
                    var t, s = b.pseudos[e] || b.setFilters[e.toLowerCase()] || oe.error("unsupported pseudo: " + e);
                    return s[C] ? s(o) : s.length > 1 ? (t = [ e, e, "", o ], b.setFilters.hasOwnProperty(e.toLowerCase()) ? ae(function(e, t) {
                        var n, i = s(e, o), r = i.length;
                        while (r--) e[n = H(e, i[r])] = !(t[n] = i[r]);
                    }) : function(e) {
                        return s(e, 0, t);
                    }) : s;
                }
            },
            pseudos: {
                not: ae(function(e) {
                    var i = [], r = [], a = d(e.replace(M, "$1"));
                    return a[C] ? ae(function(e, t, n, i) {
                        var r, o = a(e, null, i, []), s = e.length;
                        while (s--) (r = o[s]) && (e[s] = !(t[s] = r));
                    }) : function(e, t, n) {
                        return i[0] = e, a(i, null, n, r), i[0] = null, !r.pop();
                    };
                }),
                has: ae(function(t) {
                    return function(e) {
                        return oe(t, e).length > 0;
                    };
                }),
                contains: ae(function(t) {
                    return t = t.replace(K, ee), function(e) {
                        return (e.textContent || e.innerText || o(e)).indexOf(t) > -1;
                    };
                }),
                lang: ae(function(n) {
                    return U.test(n || "") || oe.error("unsupported lang: " + n), n = n.replace(K, ee).toLowerCase(), 
                    function(e) {
                        var t;
                        do {
                            if (t = T ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-");
                        } while ((e = e.parentNode) && 1 === e.nodeType);
                        return !1;
                    };
                }),
                target: function(e) {
                    var t = n.location && n.location.hash;
                    return t && t.slice(1) === e.id;
                },
                root: function(e) {
                    return e === s;
                },
                focus: function(e) {
                    return e === A.activeElement && (!A.hasFocus || A.hasFocus()) && !!(e.type || e.href || ~e.tabIndex);
                },
                enabled: pe(!1),
                disabled: pe(!0),
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected;
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected;
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
                    return !0;
                },
                parent: function(e) {
                    return !b.pseudos.empty(e);
                },
                header: function(e) {
                    return Z.test(e.nodeName);
                },
                input: function(e) {
                    return Q.test(e.nodeName);
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t;
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase());
                },
                first: he(function() {
                    return [ 0 ];
                }),
                last: he(function(e, t) {
                    return [ t - 1 ];
                }),
                eq: he(function(e, t, n) {
                    return [ n < 0 ? n + t : n ];
                }),
                even: he(function(e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e;
                }),
                odd: he(function(e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e;
                }),
                lt: he(function(e, t, n) {
                    for (var i = n < 0 ? n + t : n; --i >= 0; ) e.push(i);
                    return e;
                }),
                gt: he(function(e, t, n) {
                    for (var i = n < 0 ? n + t : n; ++i < t; ) e.push(i);
                    return e;
                })
            }
        }).pseudos.nth = b.pseudos.eq;
        for (e in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) b.pseudos[e] = de(e);
        for (e in {
            submit: !0,
            reset: !0
        }) b.pseudos[e] = fe(e);
        function ve() {}
        ve.prototype = b.filters = b.pseudos, b.setFilters = new ve(), h = oe.tokenize = function(e, t) {
            var n, i, r, o, s, a, l, c = x[e + " "];
            if (c) return t ? 0 : c.slice(0);
            s = e, a = [], l = b.preFilter;
            while (s) {
                n && !(i = $.exec(s)) || (i && (s = s.slice(i[0].length) || s), a.push(r = [])), 
                n = !1, (i = z.exec(s)) && (n = i.shift(), r.push({
                    value: n,
                    type: i[0].replace(M, " ")
                }), s = s.slice(n.length));
                for (o in b.filter) !(i = X[o].exec(s)) || l[o] && !(i = l[o](i)) || (n = i.shift(), 
                r.push({
                    value: n,
                    type: o,
                    matches: i
                }), s = s.slice(n.length));
                if (!n) break;
            }
            return t ? s.length : s ? oe.error(e) : x(e, a).slice(0);
        };
        function ye(e) {
            for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value;
            return i;
        }
        function me(a, e, t) {
            var l = e.dir, c = e.next, u = c || l, d = t && "parentNode" === u, f = i++;
            return e.first ? function(e, t, n) {
                while (e = e[l]) if (1 === e.nodeType || d) return a(e, t, n);
                return !1;
            } : function(e, t, n) {
                var i, r, o, s = [ I, f ];
                if (n) {
                    while (e = e[l]) if ((1 === e.nodeType || d) && a(e, t, n)) return !0;
                } else while (e = e[l]) if (1 === e.nodeType || d) if (o = e[C] || (e[C] = {}), 
                r = o[e.uniqueID] || (o[e.uniqueID] = {}), c && c === e.nodeName.toLowerCase()) e = e[l] || e; else {
                    if ((i = r[u]) && i[0] === I && i[1] === f) return s[2] = i[2];
                    if (r[u] = s, s[2] = a(e, t, n)) return !0;
                }
                return !1;
            };
        }
        function xe(r) {
            return r.length > 1 ? function(e, t, n) {
                var i = r.length;
                while (i--) if (!r[i](e, t, n)) return !1;
                return !0;
            } : r[0];
        }
        function be(e, t, n) {
            for (var i = 0, r = t.length; i < r; i++) oe(e, t[i], n);
            return n;
        }
        function we(e, t, n, i, r) {
            for (var o, s = [], a = 0, l = e.length, c = null != t; a < l; a++) (o = e[a]) && (n && !n(o, i, r) || (s.push(o), 
            c && t.push(a)));
            return s;
        }
        function Se(p, h, g, v, y, e) {
            return v && !v[C] && (v = Se(v)), y && !y[C] && (y = Se(y, e)), ae(function(e, t, n, i) {
                var r, o, s, a = [], l = [], c = t.length, u = e || be(h || "*", n.nodeType ? [ n ] : n, []), d = !p || !e && h ? u : we(u, a, p, n, i), f = g ? y || (e ? p : c || v) ? [] : t : d;
                if (g && g(d, f, n, i), v) {
                    r = we(f, l), v(r, [], n, i), o = r.length;
                    while (o--) (s = r[o]) && (f[l[o]] = !(d[l[o]] = s));
                }
                if (e) {
                    if (y || p) {
                        if (y) {
                            r = [], o = f.length;
                            while (o--) (s = f[o]) && r.push(d[o] = s);
                            y(null, f = [], r, i);
                        }
                        o = f.length;
                        while (o--) (s = f[o]) && (r = y ? H(e, s) : a[o]) > -1 && (e[r] = !(t[r] = s));
                    }
                } else f = we(f === t ? f.splice(c, f.length) : f), y ? y(null, t, f, i) : j.apply(t, f);
            });
        }
        function Ae(e) {
            for (var r, t, n, i = e.length, o = b.relative[e[0].type], s = o || b.relative[" "], a = o ? 1 : 0, l = me(function(e) {
                return e === r;
            }, s, !0), c = me(function(e) {
                return H(r, e) > -1;
            }, s, !0), u = [ function(e, t, n) {
                var i = !o && (n || t !== w) || ((r = t).nodeType ? l(e, t, n) : c(e, t, n));
                return r = null, i;
            } ]; a < i; a++) if (t = b.relative[e[a].type]) u = [ me(xe(u), t) ]; else {
                if ((t = b.filter[e[a].type].apply(null, e[a].matches))[C]) {
                    for (n = ++a; n < i; n++) if (b.relative[e[n].type]) break;
                    return Se(a > 1 && xe(u), a > 1 && ye(e.slice(0, a - 1).concat({
                        value: " " === e[a - 2].type ? "*" : ""
                    })).replace(M, "$1"), t, a < n && Ae(e.slice(a, n)), n < i && Ae(e = e.slice(n)), n < i && ye(e));
                }
                u.push(t);
            }
            return xe(u);
        }
        function Te(v, y) {
            var m = y.length > 0, x = v.length > 0, e = function(e, t, n, i, r) {
                var o, s, a, l = 0, c = "0", u = e && [], d = [], f = w, p = e || x && b.find.TAG("*", r), h = I += null == f ? 1 : Math.random() || .1, g = p.length;
                for (r && (w = t === A || t || r); c !== g && null != (o = p[c]); c++) {
                    if (x && o) {
                        s = 0, t || o.ownerDocument === A || (S(o), n = !T);
                        while (a = v[s++]) if (a(o, t || A, n)) {
                            i.push(o);
                            break;
                        }
                        r && (I = h);
                    }
                    m && ((o = !a && o) && l--, e && u.push(o));
                }
                if (l += c, m && c !== l) {
                    s = 0;
                    while (a = y[s++]) a(u, d, t, n);
                    if (e) {
                        if (l > 0) while (c--) u[c] || d[c] || (d[c] = D.call(i));
                        d = we(d);
                    }
                    j.apply(i, d), r && !e && d.length > 0 && l + y.length > 1 && oe.uniqueSort(i);
                }
                return r && (I = h, w = f), u;
            };
            return m ? ae(e) : e;
        }
        return d = oe.compile = function(e, t) {
            var n, i = [], r = [], o = P[e + " "];
            if (!o) {
                t || (t = h(e)), n = t.length;
                while (n--) (o = Ae(t[n]))[C] ? i.push(o) : r.push(o);
                (o = P(e, Te(r, i))).selector = e;
            }
            return o;
        }, g = oe.select = function(e, t, n, i) {
            var r, o, s, a, l, c = "function" == typeof e && e, u = !i && h(e = c.selector || e);
            if (n = n || [], 1 === u.length) {
                if ((o = u[0] = u[0].slice(0)).length > 2 && "ID" === (s = o[0]).type && 9 === t.nodeType && T && b.relative[o[1].type]) {
                    if (!(t = (b.find.ID(s.matches[0].replace(K, ee), t) || [])[0])) return n;
                    c && (t = t.parentNode), e = e.slice(o.shift().value.length);
                }
                r = X.needsContext.test(e) ? 0 : o.length;
                while (r--) {
                    if (s = o[r], b.relative[a = s.type]) break;
                    if ((l = b.find[a]) && (i = l(s.matches[0].replace(K, ee), J.test(o[0].type) && ge(t.parentNode) || t))) {
                        if (o.splice(r, 1), !(e = i.length && ye(o))) return j.apply(n, i), n;
                        break;
                    }
                }
            }
            return (c || d(e, u))(i, t, !T, n, !t || J.test(e) && ge(t.parentNode) || t), n;
        }, p.sortStable = C.split("").sort(k).join("") === C, p.detectDuplicates = !!c, 
        S(), p.sortDetached = le(function(e) {
            return 1 & e.compareDocumentPosition(A.createElement("fieldset"));
        }), le(function(e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href");
        }) || ce("type|href|height|width", function(e, t, n) {
            if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2);
        }), p.attributes && le(function(e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value");
        }) || ce("value", function(e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue;
        }), le(function(e) {
            return null == e.getAttribute("disabled");
        }) || ce(O, function(e, t, n) {
            var i;
            if (!n) return !0 === e[t] ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null;
        }), oe;
    }(A);
    C.find = h, C.expr = h.selectors, C.expr[":"] = C.expr.pseudos, C.uniqueSort = C.unique = h.uniqueSort, 
    C.text = h.getText, C.isXMLDoc = h.isXML, C.contains = h.contains, C.escapeSelector = h.escape;
    var S = function(e, t, n) {
        var i = [], r = void 0 !== n;
        while ((e = e[t]) && 9 !== e.nodeType) if (1 === e.nodeType) {
            if (r && C(e).is(n)) break;
            i.push(e);
        }
        return i;
    }, I = function(e, t) {
        for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
        return n;
    }, P = C.expr.match.needsContext;
    function k(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
    }
    var E = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
    function D(e, n, i) {
        return m(n) ? C.grep(e, function(e, t) {
            return !!n.call(e, t, e) !== i;
        }) : n.nodeType ? C.grep(e, function(e) {
            return e === n !== i;
        }) : "string" != typeof n ? C.grep(e, function(e) {
            return r.call(n, e) > -1 !== i;
        }) : C.filter(n, e, i);
    }
    C.filter = function(e, t, n) {
        var i = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? C.find.matchesSelector(i, e) ? [ i ] : [] : C.find.matches(e, C.grep(t, function(e) {
            return 1 === e.nodeType;
        }));
    }, C.fn.extend({
        find: function(e) {
            var t, n, i = this.length, r = this;
            if ("string" != typeof e) return this.pushStack(C(e).filter(function() {
                for (t = 0; t < i; t++) if (C.contains(r[t], this)) return !0;
            }));
            for (n = this.pushStack([]), t = 0; t < i; t++) C.find(e, r[t], n);
            return i > 1 ? C.uniqueSort(n) : n;
        },
        filter: function(e) {
            return this.pushStack(D(this, e || [], !1));
        },
        not: function(e) {
            return this.pushStack(D(this, e || [], !0));
        },
        is: function(e) {
            return !!D(this, "string" == typeof e && P.test(e) ? C(e) : e || [], !1).length;
        }
    });
    var N, j = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (C.fn.init = function(e, t, n) {
        var i, r;
        if (!e) return this;
        if (n = n || N, "string" == typeof e) {
            if (!(i = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [ null, e, null ] : j.exec(e)) || !i[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
            if (i[1]) {
                if (t = t instanceof C ? t[0] : t, C.merge(this, C.parseHTML(i[1], t && t.nodeType ? t.ownerDocument || t : T, !0)), 
                E.test(i[1]) && C.isPlainObject(t)) for (i in t) m(this[i]) ? this[i](t[i]) : this.attr(i, t[i]);
                return this;
            }
            return (r = T.getElementById(i[2])) && (this[0] = r, this.length = 1), this;
        }
        return e.nodeType ? (this[0] = e, this.length = 1, this) : m(e) ? void 0 !== n.ready ? n.ready(e) : e(C) : C.makeArray(e, this);
    }).prototype = C.fn, N = C(T);
    var q = /^(?:parents|prev(?:Until|All))/, H = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    C.fn.extend({
        has: function(e) {
            var t = C(e, this), n = t.length;
            return this.filter(function() {
                for (var e = 0; e < n; e++) if (C.contains(this, t[e])) return !0;
            });
        },
        closest: function(e, t) {
            var n, i = 0, r = this.length, o = [], s = "string" != typeof e && C(e);
            if (!P.test(e)) for (;i < r; i++) for (n = this[i]; n && n !== t; n = n.parentNode) if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && C.find.matchesSelector(n, e))) {
                o.push(n);
                break;
            }
            return this.pushStack(o.length > 1 ? C.uniqueSort(o) : o);
        },
        index: function(e) {
            return e ? "string" == typeof e ? r.call(C(e), this[0]) : r.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
        },
        add: function(e, t) {
            return this.pushStack(C.uniqueSort(C.merge(this.get(), C(e, t))));
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e));
        }
    });
    function O(e, t) {
        while ((e = e[t]) && 1 !== e.nodeType) ;
        return e;
    }
    C.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null;
        },
        parents: function(e) {
            return S(e, "parentNode");
        },
        parentsUntil: function(e, t, n) {
            return S(e, "parentNode", n);
        },
        next: function(e) {
            return O(e, "nextSibling");
        },
        prev: function(e) {
            return O(e, "previousSibling");
        },
        nextAll: function(e) {
            return S(e, "nextSibling");
        },
        prevAll: function(e) {
            return S(e, "previousSibling");
        },
        nextUntil: function(e, t, n) {
            return S(e, "nextSibling", n);
        },
        prevUntil: function(e, t, n) {
            return S(e, "previousSibling", n);
        },
        siblings: function(e) {
            return I((e.parentNode || {}).firstChild, e);
        },
        children: function(e) {
            return I(e.firstChild);
        },
        contents: function(e) {
            return k(e, "iframe") ? e.contentDocument : (k(e, "template") && (e = e.content || e), 
            C.merge([], e.childNodes));
        }
    }, function(i, r) {
        C.fn[i] = function(e, t) {
            var n = C.map(this, r, e);
            return "Until" !== i.slice(-5) && (t = e), t && "string" == typeof t && (n = C.filter(t, n)), 
            this.length > 1 && (H[i] || C.uniqueSort(n), q.test(i) && n.reverse()), this.pushStack(n);
        };
    });
    var L = /[^\x20\t\r\n\f]+/g;
    function _(e) {
        var n = {};
        return C.each(e.match(L) || [], function(e, t) {
            n[t] = !0;
        }), n;
    }
    C.Callbacks = function(i) {
        i = "string" == typeof i ? _(i) : C.extend({}, i);
        var n, e, t, r, o = [], s = [], a = -1, l = function() {
            for (r = r || i.once, t = n = !0; s.length; a = -1) {
                e = s.shift();
                while (++a < o.length) !1 === o[a].apply(e[0], e[1]) && i.stopOnFalse && (a = o.length, 
                e = !1);
            }
            i.memory || (e = !1), n = !1, r && (o = e ? [] : "");
        }, c = {
            add: function() {
                return o && (e && !n && (a = o.length - 1, s.push(e)), function n(e) {
                    C.each(e, function(e, t) {
                        m(t) ? i.unique && c.has(t) || o.push(t) : t && t.length && "string" !== w(t) && n(t);
                    });
                }(arguments), e && !n && l()), this;
            },
            remove: function() {
                return C.each(arguments, function(e, t) {
                    var n;
                    while ((n = C.inArray(t, o, n)) > -1) o.splice(n, 1), n <= a && a--;
                }), this;
            },
            has: function(e) {
                return e ? C.inArray(e, o) > -1 : o.length > 0;
            },
            empty: function() {
                return o && (o = []), this;
            },
            disable: function() {
                return r = s = [], o = e = "", this;
            },
            disabled: function() {
                return !o;
            },
            lock: function() {
                return r = s = [], e || n || (o = e = ""), this;
            },
            locked: function() {
                return !!r;
            },
            fireWith: function(e, t) {
                return r || (t = [ e, (t = t || []).slice ? t.slice() : t ], s.push(t), n || l()), 
                this;
            },
            fire: function() {
                return c.fireWith(this, arguments), this;
            },
            fired: function() {
                return !!t;
            }
        };
        return c;
    };
    function R(e) {
        return e;
    }
    function W(e) {
        throw e;
    }
    function F(e, t, n, i) {
        var r;
        try {
            e && m(r = e.promise) ? r.call(e).done(t).fail(n) : e && m(r = e.then) ? r.call(e, t, n) : t.apply(void 0, [ e ].slice(i));
        } catch (e) {
            n.apply(void 0, [ e ]);
        }
    }
    C.extend({
        Deferred: function(e) {
            var o = [ [ "notify", "progress", C.Callbacks("memory"), C.Callbacks("memory"), 2 ], [ "resolve", "done", C.Callbacks("once memory"), C.Callbacks("once memory"), 0, "resolved" ], [ "reject", "fail", C.Callbacks("once memory"), C.Callbacks("once memory"), 1, "rejected" ] ], r = "pending", s = {
                state: function() {
                    return r;
                },
                always: function() {
                    return a.done(arguments).fail(arguments), this;
                },
                catch: function(e) {
                    return s.then(null, e);
                },
                pipe: function() {
                    var r = arguments;
                    return C.Deferred(function(i) {
                        C.each(o, function(e, t) {
                            var n = m(r[t[4]]) && r[t[4]];
                            a[t[1]](function() {
                                var e = n && n.apply(this, arguments);
                                e && m(e.promise) ? e.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[t[0] + "With"](this, n ? [ e ] : arguments);
                            });
                        }), r = null;
                    }).promise();
                },
                then: function(t, n, i) {
                    var l = 0;
                    function c(r, o, s, a) {
                        return function() {
                            var n = this, i = arguments, e = function() {
                                var e, t;
                                if (!(r < l)) {
                                    if ((e = s.apply(n, i)) === o.promise()) throw new TypeError("Thenable self-resolution");
                                    t = e && ("object" == typeof e || "function" == typeof e) && e.then, m(t) ? a ? t.call(e, c(l, o, R, a), c(l, o, W, a)) : (l++, 
                                    t.call(e, c(l, o, R, a), c(l, o, W, a), c(l, o, R, o.notifyWith))) : (s !== R && (n = void 0, 
                                    i = [ e ]), (a || o.resolveWith)(n, i));
                                }
                            }, t = a ? e : function() {
                                try {
                                    e();
                                } catch (e) {
                                    C.Deferred.exceptionHook && C.Deferred.exceptionHook(e, t.stackTrace), r + 1 >= l && (s !== W && (n = void 0, 
                                    i = [ e ]), o.rejectWith(n, i));
                                }
                            };
                            r ? t() : (C.Deferred.getStackHook && (t.stackTrace = C.Deferred.getStackHook()), 
                            A.setTimeout(t));
                        };
                    }
                    return C.Deferred(function(e) {
                        o[0][3].add(c(0, e, m(i) ? i : R, e.notifyWith)), o[1][3].add(c(0, e, m(t) ? t : R)), 
                        o[2][3].add(c(0, e, m(n) ? n : W));
                    }).promise();
                },
                promise: function(e) {
                    return null != e ? C.extend(e, s) : s;
                }
            }, a = {};
            return C.each(o, function(e, t) {
                var n = t[2], i = t[5];
                s[t[1]] = n.add, i && n.add(function() {
                    r = i;
                }, o[3 - e][2].disable, o[3 - e][3].disable, o[0][2].lock, o[0][3].lock), n.add(t[3].fire), 
                a[t[0]] = function() {
                    return a[t[0] + "With"](this === a ? void 0 : this, arguments), this;
                }, a[t[0] + "With"] = n.fireWith;
            }), s.promise(a), e && e.call(a, a), a;
        },
        when: function(e) {
            var n = arguments.length, t = n, i = Array(t), r = a.call(arguments), o = C.Deferred(), s = function(t) {
                return function(e) {
                    i[t] = this, r[t] = arguments.length > 1 ? a.call(arguments) : e, --n || o.resolveWith(i, r);
                };
            };
            if (n <= 1 && (F(e, o.done(s(t)).resolve, o.reject, !n), "pending" === o.state() || m(r[t] && r[t].then))) return o.then();
            while (t--) F(r[t], s(t), o.reject);
            return o.promise();
        }
    });
    var M = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    C.Deferred.exceptionHook = function(e, t) {
        A.console && A.console.warn && e && M.test(e.name) && A.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t);
    }, C.readyException = function(e) {
        A.setTimeout(function() {
            throw e;
        });
    };
    var $ = C.Deferred();
    C.fn.ready = function(e) {
        return $.then(e)["catch"](function(e) {
            C.readyException(e);
        }), this;
    }, C.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(e) {
            (!0 === e ? --C.readyWait : C.isReady) || (C.isReady = !0, !0 !== e && --C.readyWait > 0 || $.resolveWith(T, [ C ]));
        }
    }), C.ready.then = $.then;
    function z() {
        T.removeEventListener("DOMContentLoaded", z), A.removeEventListener("load", z), 
        C.ready();
    }
    "complete" === T.readyState || "loading" !== T.readyState && !T.documentElement.doScroll ? A.setTimeout(C.ready) : (T.addEventListener("DOMContentLoaded", z), 
    A.addEventListener("load", z));
    var B = function(e, t, n, i, r, o, s) {
        var a = 0, l = e.length, c = null == n;
        if ("object" === w(n)) {
            r = !0;
            for (a in n) B(e, t, a, n[a], !0, o, s);
        } else if (void 0 !== i && (r = !0, m(i) || (s = !0), c && (s ? (t.call(e, i), t = null) : (c = t, 
        t = function(e, t, n) {
            return c.call(C(e), n);
        })), t)) for (;a < l; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
        return r ? e : c ? t.call(e) : l ? t(e[0], n) : o;
    }, V = /^-ms-/, U = /-([a-z])/g;
    function X(e, t) {
        return t.toUpperCase();
    }
    function Q(e) {
        return e.replace(V, "ms-").replace(U, X);
    }
    var Z = function(e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType;
    };
    function G() {
        this.expando = C.expando + G.uid++;
    }
    G.uid = 1, G.prototype = {
        cache: function(e) {
            var t = e[this.expando];
            return t || (t = {}, Z(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t;
        },
        set: function(e, t, n) {
            var i, r = this.cache(e);
            if ("string" == typeof t) r[Q(t)] = n; else for (i in t) r[Q(i)] = t[i];
            return r;
        },
        get: function(e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][Q(t)];
        },
        access: function(e, t, n) {
            return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), 
            void 0 !== n ? n : t);
        },
        remove: function(e, t) {
            var n, i = e[this.expando];
            if (void 0 !== i) {
                if (void 0 !== t) {
                    n = (t = Array.isArray(t) ? t.map(Q) : (t = Q(t)) in i ? [ t ] : t.match(L) || []).length;
                    while (n--) delete i[t[n]];
                }
                (void 0 === t || C.isEmptyObject(i)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando]);
            }
        },
        hasData: function(e) {
            var t = e[this.expando];
            return void 0 !== t && !C.isEmptyObject(t);
        }
    };
    var Y = new G(), J = new G(), K = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, ee = /[A-Z]/g;
    function te(e) {
        return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : K.test(e) ? JSON.parse(e) : e);
    }
    function ne(e, t, n) {
        var i;
        if (void 0 === n && 1 === e.nodeType) if (i = "data-" + t.replace(ee, "-$&").toLowerCase(), 
        "string" == typeof (n = e.getAttribute(i))) {
            try {
                n = te(n);
            } catch (e) {}
            J.set(e, t, n);
        } else n = void 0;
        return n;
    }
    C.extend({
        hasData: function(e) {
            return J.hasData(e) || Y.hasData(e);
        },
        data: function(e, t, n) {
            return J.access(e, t, n);
        },
        removeData: function(e, t) {
            J.remove(e, t);
        },
        _data: function(e, t, n) {
            return Y.access(e, t, n);
        },
        _removeData: function(e, t) {
            Y.remove(e, t);
        }
    }), C.fn.extend({
        data: function(n, e) {
            var t, i, r, o = this[0], s = o && o.attributes;
            if (void 0 === n) {
                if (this.length && (r = J.get(o), 1 === o.nodeType && !Y.get(o, "hasDataAttrs"))) {
                    t = s.length;
                    while (t--) s[t] && 0 === (i = s[t].name).indexOf("data-") && (i = Q(i.slice(5)), 
                    ne(o, i, r[i]));
                    Y.set(o, "hasDataAttrs", !0);
                }
                return r;
            }
            return "object" == typeof n ? this.each(function() {
                J.set(this, n);
            }) : B(this, function(e) {
                var t;
                if (o && void 0 === e) {
                    if (void 0 !== (t = J.get(o, n))) return t;
                    if (void 0 !== (t = ne(o, n))) return t;
                } else this.each(function() {
                    J.set(this, n, e);
                });
            }, null, e, arguments.length > 1, null, !0);
        },
        removeData: function(e) {
            return this.each(function() {
                J.remove(this, e);
            });
        }
    }), C.extend({
        queue: function(e, t, n) {
            var i;
            if (e) return t = (t || "fx") + "queue", i = Y.get(e, t), n && (!i || Array.isArray(n) ? i = Y.access(e, t, C.makeArray(n)) : i.push(n)), 
            i || [];
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var n = C.queue(e, t), i = n.length, r = n.shift(), o = C._queueHooks(e, t), s = function() {
                C.dequeue(e, t);
            };
            "inprogress" === r && (r = n.shift(), i--), r && ("fx" === t && n.unshift("inprogress"), 
            delete o.stop, r.call(e, s, o)), !i && o && o.empty.fire();
        },
        _queueHooks: function(e, t) {
            var n = t + "queueHooks";
            return Y.get(e, n) || Y.access(e, n, {
                empty: C.Callbacks("once memory").add(function() {
                    Y.remove(e, [ t + "queue", n ]);
                })
            });
        }
    }), C.fn.extend({
        queue: function(t, n) {
            var e = 2;
            return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? C.queue(this[0], t) : void 0 === n ? this : this.each(function() {
                var e = C.queue(this, t, n);
                C._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && C.dequeue(this, t);
            });
        },
        dequeue: function(e) {
            return this.each(function() {
                C.dequeue(this, e);
            });
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", []);
        },
        promise: function(e, t) {
            var n, i = 1, r = C.Deferred(), o = this, s = this.length, a = function() {
                --i || r.resolveWith(o, [ o ]);
            };
            "string" != typeof e && (t = e, e = void 0), e = e || "fx";
            while (s--) (n = Y.get(o[s], e + "queueHooks")) && n.empty && (i++, n.empty.add(a));
            return a(), r.promise(t);
        }
    });
    var ie = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, re = new RegExp("^(?:([+-])=|)(" + ie + ")([a-z%]*)$", "i"), oe = [ "Top", "Right", "Bottom", "Left" ], se = function(e, t) {
        return "none" === (e = t || e).style.display || "" === e.style.display && C.contains(e.ownerDocument, e) && "none" === C.css(e, "display");
    }, ae = function(e, t, n, i) {
        var r, o, s = {};
        for (o in t) s[o] = e.style[o], e.style[o] = t[o];
        r = n.apply(e, i || []);
        for (o in t) e.style[o] = s[o];
        return r;
    };
    function le(e, t, n, i) {
        var r, o, s = 20, a = i ? function() {
            return i.cur();
        } : function() {
            return C.css(e, t, "");
        }, l = a(), c = n && n[3] || (C.cssNumber[t] ? "" : "px"), u = (C.cssNumber[t] || "px" !== c && +l) && re.exec(C.css(e, t));
        if (u && u[3] !== c) {
            l /= 2, c = c || u[3], u = +l || 1;
            while (s--) C.style(e, t, u + c), (1 - o) * (1 - (o = a() / l || .5)) <= 0 && (s = 0), 
            u /= o;
            u *= 2, C.style(e, t, u + c), n = n || [];
        }
        return n && (u = +u || +l || 0, r = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, 
        i.start = u, i.end = r)), r;
    }
    var ce = {};
    function ue(e) {
        var t, n = e.ownerDocument, i = e.nodeName, r = ce[i];
        return r || (t = n.body.appendChild(n.createElement(i)), r = C.css(t, "display"), 
        t.parentNode.removeChild(t), "none" === r && (r = "block"), ce[i] = r, r);
    }
    function de(e, t) {
        for (var n, i, r = [], o = 0, s = e.length; o < s; o++) (i = e[o]).style && (n = i.style.display, 
        t ? ("none" === n && (r[o] = Y.get(i, "display") || null, r[o] || (i.style.display = "")), 
        "" === i.style.display && se(i) && (r[o] = ue(i))) : "none" !== n && (r[o] = "none", 
        Y.set(i, "display", n)));
        for (o = 0; o < s; o++) null != r[o] && (e[o].style.display = r[o]);
        return e;
    }
    C.fn.extend({
        show: function() {
            return de(this, !0);
        },
        hide: function() {
            return de(this);
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                se(this) ? C(this).show() : C(this).hide();
            });
        }
    });
    var fe = /^(?:checkbox|radio)$/i, pe = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i, he = /^$|^module$|\/(?:java|ecma)script/i, ge = {
        option: [ 1, "<select multiple='multiple'>", "</select>" ],
        thead: [ 1, "<table>", "</table>" ],
        col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
        tr: [ 2, "<table><tbody>", "</tbody></table>" ],
        td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
        _default: [ 0, "", "" ]
    };
    ge.optgroup = ge.option, ge.tbody = ge.tfoot = ge.colgroup = ge.caption = ge.thead, 
    ge.th = ge.td;
    function ve(e, t) {
        var n;
        return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], 
        void 0 === t || t && k(e, t) ? C.merge([ e ], n) : n;
    }
    function ye(e, t) {
        for (var n = 0, i = e.length; n < i; n++) Y.set(e[n], "globalEval", !t || Y.get(t[n], "globalEval"));
    }
    var me = /<|&#?\w+;/;
    function xe(e, t, n, i, r) {
        for (var o, s, a, l, c, u, d = t.createDocumentFragment(), f = [], p = 0, h = e.length; p < h; p++) if ((o = e[p]) || 0 === o) if ("object" === w(o)) C.merge(f, o.nodeType ? [ o ] : o); else if (me.test(o)) {
            s = s || d.appendChild(t.createElement("div")), a = (pe.exec(o) || [ "", "" ])[1].toLowerCase(), 
            l = ge[a] || ge._default, s.innerHTML = l[1] + C.htmlPrefilter(o) + l[2], u = l[0];
            while (u--) s = s.lastChild;
            C.merge(f, s.childNodes), (s = d.firstChild).textContent = "";
        } else f.push(t.createTextNode(o));
        d.textContent = "", p = 0;
        while (o = f[p++]) if (i && C.inArray(o, i) > -1) r && r.push(o); else if (c = C.contains(o.ownerDocument, o), 
        s = ve(d.appendChild(o), "script"), c && ye(s), n) {
            u = 0;
            while (o = s[u++]) he.test(o.type || "") && n.push(o);
        }
        return d;
    }
    !function() {
        var e = T.createDocumentFragment().appendChild(T.createElement("div")), t = T.createElement("input");
        t.setAttribute("type", "radio"), t.setAttribute("checked", "checked"), t.setAttribute("name", "t"), 
        e.appendChild(t), y.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, 
        e.innerHTML = "<textarea>x</textarea>", y.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue;
    }();
    var be = T.documentElement, we = /^key/, Se = /^(?:mouse|pointer|contextmenu|drag|drop)|click/, Ae = /^([^.]*)(?:\.(.+)|)/;
    function Te() {
        return !0;
    }
    function Ce() {
        return !1;
    }
    function Ie() {
        try {
            return T.activeElement;
        } catch (e) {}
    }
    function Pe(e, t, n, i, r, o) {
        var s, a;
        if ("object" == typeof t) {
            "string" != typeof n && (i = i || n, n = void 0);
            for (a in t) Pe(e, a, n, i, t[a], o);
            return e;
        }
        if (null == i && null == r ? (r = n, i = n = void 0) : null == r && ("string" == typeof n ? (r = i, 
        i = void 0) : (r = i, i = n, n = void 0)), !1 === r) r = Ce; else if (!r) return e;
        return 1 === o && (s = r, (r = function(e) {
            return C().off(e), s.apply(this, arguments);
        }).guid = s.guid || (s.guid = C.guid++)), e.each(function() {
            C.event.add(this, t, r, i, n);
        });
    }
    C.event = {
        global: {},
        add: function(t, e, n, i, r) {
            var o, s, a, l, c, u, d, f, p, h, g, v = Y.get(t);
            if (v) {
                n.handler && (n = (o = n).handler, r = o.selector), r && C.find.matchesSelector(be, r), 
                n.guid || (n.guid = C.guid++), (l = v.events) || (l = v.events = {}), (s = v.handle) || (s = v.handle = function(e) {
                    return "undefined" != typeof C && C.event.triggered !== e.type ? C.event.dispatch.apply(t, arguments) : void 0;
                }), c = (e = (e || "").match(L) || [ "" ]).length;
                while (c--) p = g = (a = Ae.exec(e[c]) || [])[1], h = (a[2] || "").split(".").sort(), 
                p && (d = C.event.special[p] || {}, p = (r ? d.delegateType : d.bindType) || p, 
                d = C.event.special[p] || {}, u = C.extend({
                    type: p,
                    origType: g,
                    data: i,
                    handler: n,
                    guid: n.guid,
                    selector: r,
                    needsContext: r && C.expr.match.needsContext.test(r),
                    namespace: h.join(".")
                }, o), (f = l[p]) || ((f = l[p] = []).delegateCount = 0, d.setup && !1 !== d.setup.call(t, i, h, s) || t.addEventListener && t.addEventListener(p, s)), 
                d.add && (d.add.call(t, u), u.handler.guid || (u.handler.guid = n.guid)), r ? f.splice(f.delegateCount++, 0, u) : f.push(u), 
                C.event.global[p] = !0);
            }
        },
        remove: function(e, t, n, i, r) {
            var o, s, a, l, c, u, d, f, p, h, g, v = Y.hasData(e) && Y.get(e);
            if (v && (l = v.events)) {
                c = (t = (t || "").match(L) || [ "" ]).length;
                while (c--) if (a = Ae.exec(t[c]) || [], p = g = a[1], h = (a[2] || "").split(".").sort(), 
                p) {
                    d = C.event.special[p] || {}, f = l[p = (i ? d.delegateType : d.bindType) || p] || [], 
                    a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = o = f.length;
                    while (o--) u = f[o], !r && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (f.splice(o, 1), 
                    u.selector && f.delegateCount--, d.remove && d.remove.call(e, u));
                    s && !f.length && (d.teardown && !1 !== d.teardown.call(e, h, v.handle) || C.removeEvent(e, p, v.handle), 
                    delete l[p]);
                } else for (p in l) C.event.remove(e, p + t[c], n, i, !0);
                C.isEmptyObject(l) && Y.remove(e, "handle events");
            }
        },
        dispatch: function(e) {
            var t = C.event.fix(e), n, i, r, o, s, a, l = new Array(arguments.length), c = (Y.get(this, "events") || {})[t.type] || [], u = C.event.special[t.type] || {};
            for (l[0] = t, n = 1; n < arguments.length; n++) l[n] = arguments[n];
            if (t.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, t)) {
                a = C.event.handlers.call(this, t, c), n = 0;
                while ((o = a[n++]) && !t.isPropagationStopped()) {
                    t.currentTarget = o.elem, i = 0;
                    while ((s = o.handlers[i++]) && !t.isImmediatePropagationStopped()) t.rnamespace && !t.rnamespace.test(s.namespace) || (t.handleObj = s, 
                    t.data = s.data, void 0 !== (r = ((C.event.special[s.origType] || {}).handle || s.handler).apply(o.elem, l)) && !1 === (t.result = r) && (t.preventDefault(), 
                    t.stopPropagation()));
                }
                return u.postDispatch && u.postDispatch.call(this, t), t.result;
            }
        },
        handlers: function(e, t) {
            var n, i, r, o, s, a = [], l = t.delegateCount, c = e.target;
            if (l && c.nodeType && !("click" === e.type && e.button >= 1)) for (;c !== this; c = c.parentNode || this) if (1 === c.nodeType && ("click" !== e.type || !0 !== c.disabled)) {
                for (o = [], s = {}, n = 0; n < l; n++) void 0 === s[r = (i = t[n]).selector + " "] && (s[r] = i.needsContext ? C(r, this).index(c) > -1 : C.find(r, this, null, [ c ]).length), 
                s[r] && o.push(i);
                o.length && a.push({
                    elem: c,
                    handlers: o
                });
            }
            return c = this, l < t.length && a.push({
                elem: c,
                handlers: t.slice(l)
            }), a;
        },
        addProp: function(t, e) {
            Object.defineProperty(C.Event.prototype, t, {
                enumerable: !0,
                configurable: !0,
                get: m(e) ? function() {
                    if (this.originalEvent) return e(this.originalEvent);
                } : function() {
                    if (this.originalEvent) return this.originalEvent[t];
                },
                set: function(e) {
                    Object.defineProperty(this, t, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: e
                    });
                }
            });
        },
        fix: function(e) {
            return e[C.expando] ? e : new C.Event(e);
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== Ie() && this.focus) return this.focus(), !1;
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === Ie() && this.blur) return this.blur(), !1;
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && k(this, "input")) return this.click(), 
                    !1;
                },
                _default: function(e) {
                    return k(e.target, "a");
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result);
                }
            }
        }
    }, C.removeEvent = function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n);
    }, C.Event = function(e, t) {
        if (!(this instanceof C.Event)) return new C.Event(e, t);
        e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? Te : Ce, 
        this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, 
        this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, 
        t && C.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[C.expando] = !0;
    }, C.Event.prototype = {
        constructor: C.Event,
        isDefaultPrevented: Ce,
        isPropagationStopped: Ce,
        isImmediatePropagationStopped: Ce,
        isSimulated: !1,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = Te, e && !this.isSimulated && e.preventDefault();
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = Te, e && !this.isSimulated && e.stopPropagation();
        },
        stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = Te, e && !this.isSimulated && e.stopImmediatePropagation(), 
            this.stopPropagation();
        }
    }, C.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(e) {
            var t = e.button;
            return null == e.which && we.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && Se.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which;
        }
    }, C.event.addProp), C.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(e, o) {
        C.event.special[e] = {
            delegateType: o,
            bindType: o,
            handle: function(e) {
                var t, n = this, i = e.relatedTarget, r = e.handleObj;
                return i && (i === n || C.contains(n, i)) || (e.type = r.origType, t = r.handler.apply(this, arguments), 
                e.type = o), t;
            }
        };
    }), C.fn.extend({
        on: function(e, t, n, i) {
            return Pe(this, e, t, n, i);
        },
        one: function(e, t, n, i) {
            return Pe(this, e, t, n, i, 1);
        },
        off: function(e, t, n) {
            var i, r;
            if (e && e.preventDefault && e.handleObj) return i = e.handleObj, C(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), 
            this;
            if ("object" == typeof e) {
                for (r in e) this.off(r, t, e[r]);
                return this;
            }
            return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = Ce), 
            this.each(function() {
                C.event.remove(this, e, n, t);
            });
        }
    });
    var ke = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi, Ee = /<script|<style|<link/i, De = /checked\s*(?:[^=]|=\s*.checked.)/i, Ne = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    function je(e, t) {
        return k(e, "table") && k(11 !== t.nodeType ? t : t.firstChild, "tr") ? C(e).children("tbody")[0] || e : e;
    }
    function qe(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e;
    }
    function He(e) {
        return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), 
        e;
    }
    function Oe(e, t) {
        var n, i, r, o, s, a, l, c;
        if (1 === t.nodeType) {
            if (Y.hasData(e) && (o = Y.access(e), s = Y.set(t, o), c = o.events)) {
                delete s.handle, s.events = {};
                for (r in c) for (n = 0, i = c[r].length; n < i; n++) C.event.add(t, r, c[r][n]);
            }
            J.hasData(e) && (a = J.access(e), l = C.extend({}, a), J.set(t, l));
        }
    }
    function Le(e, t) {
        var n = t.nodeName.toLowerCase();
        "input" === n && fe.test(e.type) ? t.checked = e.checked : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue);
    }
    function _e(n, i, r, o) {
        i = g.apply([], i);
        var e, t, s, a, l, c, u = 0, d = n.length, f = d - 1, p = i[0], h = m(p);
        if (h || d > 1 && "string" == typeof p && !y.checkClone && De.test(p)) return n.each(function(e) {
            var t = n.eq(e);
            h && (i[0] = p.call(this, e, t.html())), _e(t, i, r, o);
        });
        if (d && (e = xe(i, n[0].ownerDocument, !1, n, o), t = e.firstChild, 1 === e.childNodes.length && (e = t), 
        t || o)) {
            for (a = (s = C.map(ve(e, "script"), qe)).length; u < d; u++) l = e, u !== f && (l = C.clone(l, !0, !0), 
            a && C.merge(s, ve(l, "script"))), r.call(n[u], l, u);
            if (a) for (c = s[s.length - 1].ownerDocument, C.map(s, He), u = 0; u < a; u++) l = s[u], 
            he.test(l.type || "") && !Y.access(l, "globalEval") && C.contains(c, l) && (l.src && "module" !== (l.type || "").toLowerCase() ? C._evalUrl && C._evalUrl(l.src) : b(l.textContent.replace(Ne, ""), c, l));
        }
        return n;
    }
    function Re(e, t, n) {
        for (var i, r = t ? C.filter(t, e) : e, o = 0; null != (i = r[o]); o++) n || 1 !== i.nodeType || C.cleanData(ve(i)), 
        i.parentNode && (n && C.contains(i.ownerDocument, i) && ye(ve(i, "script")), i.parentNode.removeChild(i));
        return e;
    }
    C.extend({
        htmlPrefilter: function(e) {
            return e.replace(ke, "<$1></$2>");
        },
        clone: function(e, t, n) {
            var i, r, o, s, a = e.cloneNode(!0), l = C.contains(e.ownerDocument, e);
            if (!(y.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || C.isXMLDoc(e))) for (s = ve(a), 
            i = 0, r = (o = ve(e)).length; i < r; i++) Le(o[i], s[i]);
            if (t) if (n) for (o = o || ve(e), s = s || ve(a), i = 0, r = o.length; i < r; i++) Oe(o[i], s[i]); else Oe(e, a);
            return (s = ve(a, "script")).length > 0 && ye(s, !l && ve(e, "script")), a;
        },
        cleanData: function(e) {
            for (var t, n, i, r = C.event.special, o = 0; void 0 !== (n = e[o]); o++) if (Z(n)) {
                if (t = n[Y.expando]) {
                    if (t.events) for (i in t.events) r[i] ? C.event.remove(n, i) : C.removeEvent(n, i, t.handle);
                    n[Y.expando] = void 0;
                }
                n[J.expando] && (n[J.expando] = void 0);
            }
        }
    }), C.fn.extend({
        detach: function(e) {
            return Re(this, e, !0);
        },
        remove: function(e) {
            return Re(this, e);
        },
        text: function(e) {
            return B(this, function(e) {
                return void 0 === e ? C.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e);
                });
            }, null, e, arguments.length);
        },
        append: function() {
            return _e(this, arguments, function(e) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || je(this, e).appendChild(e);
            });
        },
        prepend: function() {
            return _e(this, arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = je(this, e);
                    t.insertBefore(e, t.firstChild);
                }
            });
        },
        before: function() {
            return _e(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this);
            });
        },
        after: function() {
            return _e(this, arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling);
            });
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (C.cleanData(ve(e, !1)), 
            e.textContent = "");
            return this;
        },
        clone: function(e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function() {
                return C.clone(this, e, t);
            });
        },
        html: function(e) {
            return B(this, function(e) {
                var t = this[0] || {}, n = 0, i = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !Ee.test(e) && !ge[(pe.exec(e) || [ "", "" ])[1].toLowerCase()]) {
                    e = C.htmlPrefilter(e);
                    try {
                        for (;n < i; n++) 1 === (t = this[n] || {}).nodeType && (C.cleanData(ve(t, !1)), 
                        t.innerHTML = e);
                        t = 0;
                    } catch (e) {}
                }
                t && this.empty().append(e);
            }, null, e, arguments.length);
        },
        replaceWith: function() {
            var n = [];
            return _e(this, arguments, function(e) {
                var t = this.parentNode;
                C.inArray(this, n) < 0 && (C.cleanData(ve(this)), t && t.replaceChild(e, this));
            }, n);
        }
    }), C.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, s) {
        C.fn[e] = function(e) {
            for (var t, n = [], i = C(e), r = i.length - 1, o = 0; o <= r; o++) t = o === r ? this : this.clone(!0), 
            C(i[o])[s](t), l.apply(n, t.get());
            return this.pushStack(n);
        };
    });
    var We = new RegExp("^(" + ie + ")(?!px)[a-z%]+$", "i"), Fe = function(e) {
        var t = e.ownerDocument.defaultView;
        return t && t.opener || (t = A), t.getComputedStyle(e);
    }, Me = new RegExp(oe.join("|"), "i");
    !function() {
        function e() {
            if (l) {
                a.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", 
                l.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", 
                be.appendChild(a).appendChild(l);
                var e = A.getComputedStyle(l);
                n = "1%" !== e.top, s = 12 === t(e.marginLeft), l.style.right = "60%", o = 36 === t(e.right), 
                i = 36 === t(e.width), l.style.position = "absolute", r = 36 === l.offsetWidth || "absolute", 
                be.removeChild(a), l = null;
            }
        }
        function t(e) {
            return Math.round(parseFloat(e));
        }
        var n, i, r, o, s, a = T.createElement("div"), l = T.createElement("div");
        l.style && (l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", 
        y.clearCloneStyle = "content-box" === l.style.backgroundClip, C.extend(y, {
            boxSizingReliable: function() {
                return e(), i;
            },
            pixelBoxStyles: function() {
                return e(), o;
            },
            pixelPosition: function() {
                return e(), n;
            },
            reliableMarginLeft: function() {
                return e(), s;
            },
            scrollboxSize: function() {
                return e(), r;
            }
        }));
    }();
    function $e(e, t, n) {
        var i, r, o, s, a = e.style;
        return (n = n || Fe(e)) && ("" !== (s = n.getPropertyValue(t) || n[t]) || C.contains(e.ownerDocument, e) || (s = C.style(e, t)), 
        !y.pixelBoxStyles() && We.test(s) && Me.test(t) && (i = a.width, r = a.minWidth, 
        o = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, 
        a.minWidth = r, a.maxWidth = o)), void 0 !== s ? s + "" : s;
    }
    function ze(e, t) {
        return {
            get: function() {
                if (!e()) return (this.get = t).apply(this, arguments);
                delete this.get;
            }
        };
    }
    var Be = /^(none|table(?!-c[ea]).+)/, Ve = /^--/, Ue = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, Xe = {
        letterSpacing: "0",
        fontWeight: "400"
    }, Qe = [ "Webkit", "Moz", "ms" ], Ze = T.createElement("div").style;
    function Ge(e) {
        if (e in Ze) return e;
        var t = e[0].toUpperCase() + e.slice(1), n = Qe.length;
        while (n--) if ((e = Qe[n] + t) in Ze) return e;
    }
    function Ye(e) {
        var t = C.cssProps[e];
        return t || (t = C.cssProps[e] = Ge(e) || e), t;
    }
    function Je(e, t, n) {
        var i = re.exec(t);
        return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : t;
    }
    function Ke(e, t, n, i, r, o) {
        var s = "width" === t ? 1 : 0, a = 0, l = 0;
        if (n === (i ? "border" : "content")) return 0;
        for (;s < 4; s += 2) "margin" === n && (l += C.css(e, n + oe[s], !0, r)), i ? ("content" === n && (l -= C.css(e, "padding" + oe[s], !0, r)), 
        "margin" !== n && (l -= C.css(e, "border" + oe[s] + "Width", !0, r))) : (l += C.css(e, "padding" + oe[s], !0, r), 
        "padding" !== n ? l += C.css(e, "border" + oe[s] + "Width", !0, r) : a += C.css(e, "border" + oe[s] + "Width", !0, r));
        return !i && o >= 0 && (l += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - l - a - .5))), 
        l;
    }
    function et(e, t, n) {
        var i = Fe(e), r = $e(e, t, i), o = "border-box" === C.css(e, "boxSizing", !1, i), s = o;
        if (We.test(r)) {
            if (!n) return r;
            r = "auto";
        }
        return s = s && (y.boxSizingReliable() || r === e.style[t]), ("auto" === r || !parseFloat(r) && "inline" === C.css(e, "display", !1, i)) && (r = e["offset" + t[0].toUpperCase() + t.slice(1)], 
        s = !0), (r = parseFloat(r) || 0) + Ke(e, t, n || (o ? "border" : "content"), s, i, r) + "px";
    }
    C.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var n = $e(e, "opacity");
                        return "" === n ? "1" : n;
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {},
        style: function(e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var r, o, s, a = Q(t), l = Ve.test(t), c = e.style;
                if (l || (t = Ye(a)), s = C.cssHooks[t] || C.cssHooks[a], void 0 === n) return s && "get" in s && void 0 !== (r = s.get(e, !1, i)) ? r : c[t];
                "string" == (o = typeof n) && (r = re.exec(n)) && r[1] && (n = le(e, t, r), o = "number"), 
                null != n && n === n && ("number" === o && (n += r && r[3] || (C.cssNumber[a] ? "" : "px")), 
                y.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"), 
                s && "set" in s && void 0 === (n = s.set(e, n, i)) || (l ? c.setProperty(t, n) : c[t] = n));
            }
        },
        css: function(e, t, n, i) {
            var r, o, s, a = Q(t);
            return Ve.test(t) || (t = Ye(a)), (s = C.cssHooks[t] || C.cssHooks[a]) && "get" in s && (r = s.get(e, !0, n)), 
            void 0 === r && (r = $e(e, t, i)), "normal" === r && t in Xe && (r = Xe[t]), "" === n || n ? (o = parseFloat(r), 
            !0 === n || isFinite(o) ? o || 0 : r) : r;
        }
    }), C.each([ "height", "width" ], function(e, a) {
        C.cssHooks[a] = {
            get: function(e, t, n) {
                if (t) return !Be.test(C.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? et(e, a, n) : ae(e, Ue, function() {
                    return et(e, a, n);
                });
            },
            set: function(e, t, n) {
                var i, r = Fe(e), o = "border-box" === C.css(e, "boxSizing", !1, r), s = n && Ke(e, a, n, o, r);
                return o && y.scrollboxSize() === r.position && (s -= Math.ceil(e["offset" + a[0].toUpperCase() + a.slice(1)] - parseFloat(r[a]) - Ke(e, a, "border", !1, r) - .5)), 
                s && (i = re.exec(t)) && "px" !== (i[3] || "px") && (e.style[a] = t, t = C.css(e, a)), 
                Je(e, t, s);
            }
        };
    }), C.cssHooks.marginLeft = ze(y.reliableMarginLeft, function(e, t) {
        if (t) return (parseFloat($e(e, "marginLeft")) || e.getBoundingClientRect().left - ae(e, {
            marginLeft: 0
        }, function() {
            return e.getBoundingClientRect().left;
        })) + "px";
    }), C.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(r, o) {
        C.cssHooks[r + o] = {
            expand: function(e) {
                for (var t = 0, n = {}, i = "string" == typeof e ? e.split(" ") : [ e ]; t < 4; t++) n[r + oe[t] + o] = i[t] || i[t - 2] || i[0];
                return n;
            }
        }, "margin" !== r && (C.cssHooks[r + o].set = Je);
    }), C.fn.extend({
        css: function(e, t) {
            return B(this, function(e, t, n) {
                var i, r, o = {}, s = 0;
                if (Array.isArray(t)) {
                    for (i = Fe(e), r = t.length; s < r; s++) o[t[s]] = C.css(e, t[s], !1, i);
                    return o;
                }
                return void 0 !== n ? C.style(e, t, n) : C.css(e, t);
            }, e, t, arguments.length > 1);
        }
    });
    function tt(e, t, n, i, r) {
        return new tt.prototype.init(e, t, n, i, r);
    }
    C.Tween = tt, tt.prototype = {
        constructor: tt,
        init: function(e, t, n, i, r, o) {
            this.elem = e, this.prop = n, this.easing = r || C.easing._default, this.options = t, 
            this.start = this.now = this.cur(), this.end = i, this.unit = o || (C.cssNumber[n] ? "" : "px");
        },
        cur: function() {
            var e = tt.propHooks[this.prop];
            return e && e.get ? e.get(this) : tt.propHooks._default.get(this);
        },
        run: function(e) {
            var t, n = tt.propHooks[this.prop];
            return this.options.duration ? this.pos = t = C.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, 
            this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), 
            n && n.set ? n.set(this) : tt.propHooks._default.set(this), this;
        }
    }, tt.prototype.init.prototype = tt.prototype, tt.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = C.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0;
            },
            set: function(e) {
                C.fx.step[e.prop] ? C.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[C.cssProps[e.prop]] && !C.cssHooks[e.prop] ? e.elem[e.prop] = e.now : C.style(e.elem, e.prop, e.now + e.unit);
            }
        }
    }, tt.propHooks.scrollTop = tt.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
        }
    }, C.easing = {
        linear: function(e) {
            return e;
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2;
        },
        _default: "swing"
    }, C.fx = tt.prototype.init, C.fx.step = {};
    var nt, it, rt = /^(?:toggle|show|hide)$/, ot = /queueHooks$/;
    function st() {
        it && (!1 === T.hidden && A.requestAnimationFrame ? A.requestAnimationFrame(st) : A.setTimeout(st, C.fx.interval), 
        C.fx.tick());
    }
    function at() {
        return A.setTimeout(function() {
            nt = void 0;
        }), nt = Date.now();
    }
    function lt(e, t) {
        var n, i = 0, r = {
            height: e
        };
        for (t = t ? 1 : 0; i < 4; i += 2 - t) r["margin" + (n = oe[i])] = r["padding" + n] = e;
        return t && (r.opacity = r.width = e), r;
    }
    function ct(e, t, n) {
        for (var i, r = (ft.tweeners[t] || []).concat(ft.tweeners["*"]), o = 0, s = r.length; o < s; o++) if (i = r[o].call(n, t, e)) return i;
    }
    function ut(e, t, n) {
        var i, r, o, s, a, l, c, u, d = "width" in t || "height" in t, f = this, p = {}, h = e.style, g = e.nodeType && se(e), v = Y.get(e, "fxshow");
        n.queue || (null == (s = C._queueHooks(e, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, 
        s.empty.fire = function() {
            s.unqueued || a();
        }), s.unqueued++, f.always(function() {
            f.always(function() {
                s.unqueued--, C.queue(e, "fx").length || s.empty.fire();
            });
        }));
        for (i in t) if (r = t[i], rt.test(r)) {
            if (delete t[i], o = o || "toggle" === r, r === (g ? "hide" : "show")) {
                if ("show" !== r || !v || void 0 === v[i]) continue;
                g = !0;
            }
            p[i] = v && v[i] || C.style(e, i);
        }
        if ((l = !C.isEmptyObject(t)) || !C.isEmptyObject(p)) {
            d && 1 === e.nodeType && (n.overflow = [ h.overflow, h.overflowX, h.overflowY ], 
            null == (c = v && v.display) && (c = Y.get(e, "display")), "none" === (u = C.css(e, "display")) && (c ? u = c : (de([ e ], !0), 
            c = e.style.display || c, u = C.css(e, "display"), de([ e ]))), ("inline" === u || "inline-block" === u && null != c) && "none" === C.css(e, "float") && (l || (f.done(function() {
                h.display = c;
            }), null == c && (u = h.display, c = "none" === u ? "" : u)), h.display = "inline-block")), 
            n.overflow && (h.overflow = "hidden", f.always(function() {
                h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2];
            })), l = !1;
            for (i in p) l || (v ? "hidden" in v && (g = v.hidden) : v = Y.access(e, "fxshow", {
                display: c
            }), o && (v.hidden = !g), g && de([ e ], !0), f.done(function() {
                g || de([ e ]), Y.remove(e, "fxshow");
                for (i in p) C.style(e, i, p[i]);
            })), l = ct(g ? v[i] : 0, i, f), i in v || (v[i] = l.start, g && (l.end = l.start, 
            l.start = 0));
        }
    }
    function dt(e, t) {
        var n, i, r, o, s;
        for (n in e) if (i = Q(n), r = t[i], o = e[n], Array.isArray(o) && (r = o[1], o = e[n] = o[0]), 
        n !== i && (e[i] = o, delete e[n]), (s = C.cssHooks[i]) && "expand" in s) {
            o = s.expand(o), delete e[i];
            for (n in o) n in e || (e[n] = o[n], t[n] = r);
        } else t[i] = r;
    }
    function ft(o, e, t) {
        var n, s, i = 0, r = ft.prefilters.length, a = C.Deferred().always(function() {
            delete l.elem;
        }), l = function() {
            if (s) return !1;
            for (var e = nt || at(), t = Math.max(0, c.startTime + c.duration - e), n = 1 - (t / c.duration || 0), i = 0, r = c.tweens.length; i < r; i++) c.tweens[i].run(n);
            return a.notifyWith(o, [ c, n, t ]), n < 1 && r ? t : (r || a.notifyWith(o, [ c, 1, 0 ]), 
            a.resolveWith(o, [ c ]), !1);
        }, c = a.promise({
            elem: o,
            props: C.extend({}, e),
            opts: C.extend(!0, {
                specialEasing: {},
                easing: C.easing._default
            }, t),
            originalProperties: e,
            originalOptions: t,
            startTime: nt || at(),
            duration: t.duration,
            tweens: [],
            createTween: function(e, t) {
                var n = C.Tween(o, c.opts, e, t, c.opts.specialEasing[e] || c.opts.easing);
                return c.tweens.push(n), n;
            },
            stop: function(e) {
                var t = 0, n = e ? c.tweens.length : 0;
                if (s) return this;
                for (s = !0; t < n; t++) c.tweens[t].run(1);
                return e ? (a.notifyWith(o, [ c, 1, 0 ]), a.resolveWith(o, [ c, e ])) : a.rejectWith(o, [ c, e ]), 
                this;
            }
        }), u = c.props;
        for (dt(u, c.opts.specialEasing); i < r; i++) if (n = ft.prefilters[i].call(c, o, u, c.opts)) return m(n.stop) && (C._queueHooks(c.elem, c.opts.queue).stop = n.stop.bind(n)), 
        n;
        return C.map(u, ct, c), m(c.opts.start) && c.opts.start.call(o, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), 
        C.fx.timer(C.extend(l, {
            elem: o,
            anim: c,
            queue: c.opts.queue
        })), c;
    }
    C.Animation = C.extend(ft, {
        tweeners: {
            "*": [ function(e, t) {
                var n = this.createTween(e, t);
                return le(n.elem, e, re.exec(t), n), n;
            } ]
        },
        tweener: function(e, t) {
            m(e) ? (t = e, e = [ "*" ]) : e = e.match(L);
            for (var n, i = 0, r = e.length; i < r; i++) n = e[i], ft.tweeners[n] = ft.tweeners[n] || [], 
            ft.tweeners[n].unshift(t);
        },
        prefilters: [ ut ],
        prefilter: function(e, t) {
            t ? ft.prefilters.unshift(e) : ft.prefilters.push(e);
        }
    }), C.speed = function(e, t, n) {
        var i = e && "object" == typeof e ? C.extend({}, e) : {
            complete: n || !n && t || m(e) && e,
            duration: e,
            easing: n && t || t && !m(t) && t
        };
        return C.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in C.fx.speeds ? i.duration = C.fx.speeds[i.duration] : i.duration = C.fx.speeds._default), 
        null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function() {
            m(i.old) && i.old.call(this), i.queue && C.dequeue(this, i.queue);
        }, i;
    }, C.fn.extend({
        fadeTo: function(e, t, n, i) {
            return this.filter(se).css("opacity", 0).show().end().animate({
                opacity: t
            }, e, n, i);
        },
        animate: function(t, e, n, i) {
            var r = C.isEmptyObject(t), o = C.speed(e, n, i), s = function() {
                var e = ft(this, C.extend({}, t), o);
                (r || Y.get(this, "finish")) && e.stop(!0);
            };
            return s.finish = s, r || !1 === o.queue ? this.each(s) : this.queue(o.queue, s);
        },
        stop: function(r, e, o) {
            var s = function(e) {
                var t = e.stop;
                delete e.stop, t(o);
            };
            return "string" != typeof r && (o = e, e = r, r = void 0), e && !1 !== r && this.queue(r || "fx", []), 
            this.each(function() {
                var e = !0, t = null != r && r + "queueHooks", n = C.timers, i = Y.get(this);
                if (t) i[t] && i[t].stop && s(i[t]); else for (t in i) i[t] && i[t].stop && ot.test(t) && s(i[t]);
                for (t = n.length; t--; ) n[t].elem !== this || null != r && n[t].queue !== r || (n[t].anim.stop(o), 
                e = !1, n.splice(t, 1));
                !e && o || C.dequeue(this, r);
            });
        },
        finish: function(s) {
            return !1 !== s && (s = s || "fx"), this.each(function() {
                var e, t = Y.get(this), n = t[s + "queue"], i = t[s + "queueHooks"], r = C.timers, o = n ? n.length : 0;
                for (t.finish = !0, C.queue(this, s, []), i && i.stop && i.stop.call(this, !0), 
                e = r.length; e--; ) r[e].elem === this && r[e].queue === s && (r[e].anim.stop(!0), 
                r.splice(e, 1));
                for (e = 0; e < o; e++) n[e] && n[e].finish && n[e].finish.call(this);
                delete t.finish;
            });
        }
    }), C.each([ "toggle", "show", "hide" ], function(e, i) {
        var r = C.fn[i];
        C.fn[i] = function(e, t, n) {
            return null == e || "boolean" == typeof e ? r.apply(this, arguments) : this.animate(lt(i, !0), e, t, n);
        };
    }), C.each({
        slideDown: lt("show"),
        slideUp: lt("hide"),
        slideToggle: lt("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(e, i) {
        C.fn[e] = function(e, t, n) {
            return this.animate(i, e, t, n);
        };
    }), C.timers = [], C.fx.tick = function() {
        var e, t = 0, n = C.timers;
        for (nt = Date.now(); t < n.length; t++) (e = n[t])() || n[t] !== e || n.splice(t--, 1);
        n.length || C.fx.stop(), nt = void 0;
    }, C.fx.timer = function(e) {
        C.timers.push(e), C.fx.start();
    }, C.fx.interval = 13, C.fx.start = function() {
        it || (it = !0, st());
    }, C.fx.stop = function() {
        it = null;
    }, C.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, C.fn.delay = function(i, e) {
        return i = C.fx ? C.fx.speeds[i] || i : i, e = e || "fx", this.queue(e, function(e, t) {
            var n = A.setTimeout(e, i);
            t.stop = function() {
                A.clearTimeout(n);
            };
        });
    }, function() {
        var e = T.createElement("input"), t = T.createElement("select").appendChild(T.createElement("option"));
        e.type = "checkbox", y.checkOn = "" !== e.value, y.optSelected = t.selected, (e = T.createElement("input")).value = "t", 
        e.type = "radio", y.radioValue = "t" === e.value;
    }();
    var pt, ht = C.expr.attrHandle;
    C.fn.extend({
        attr: function(e, t) {
            return B(this, C.attr, e, t, arguments.length > 1);
        },
        removeAttr: function(e) {
            return this.each(function() {
                C.removeAttr(this, e);
            });
        }
    }), C.extend({
        attr: function(e, t, n) {
            var i, r, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return "undefined" == typeof e.getAttribute ? C.prop(e, t, n) : (1 === o && C.isXMLDoc(e) || (r = C.attrHooks[t.toLowerCase()] || (C.expr.match.bool.test(t) ? pt : void 0)), 
            void 0 !== n ? null === n ? void C.removeAttr(e, t) : r && "set" in r && void 0 !== (i = r.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), 
            n) : r && "get" in r && null !== (i = r.get(e, t)) ? i : null == (i = C.find.attr(e, t)) ? void 0 : i);
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!y.radioValue && "radio" === t && k(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t;
                    }
                }
            }
        },
        removeAttr: function(e, t) {
            var n, i = 0, r = t && t.match(L);
            if (r && 1 === e.nodeType) while (n = r[i++]) e.removeAttribute(n);
        }
    }), pt = {
        set: function(e, t, n) {
            return !1 === t ? C.removeAttr(e, n) : e.setAttribute(n, n), n;
        }
    }, C.each(C.expr.match.bool.source.match(/\w+/g), function(e, t) {
        var s = ht[t] || C.find.attr;
        ht[t] = function(e, t, n) {
            var i, r, o = t.toLowerCase();
            return n || (r = ht[o], ht[o] = i, i = null != s(e, t, n) ? o : null, ht[o] = r), 
            i;
        };
    });
    var gt = /^(?:input|select|textarea|button)$/i, vt = /^(?:a|area)$/i;
    C.fn.extend({
        prop: function(e, t) {
            return B(this, C.prop, e, t, arguments.length > 1);
        },
        removeProp: function(e) {
            return this.each(function() {
                delete this[C.propFix[e] || e];
            });
        }
    }), C.extend({
        prop: function(e, t, n) {
            var i, r, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return 1 === o && C.isXMLDoc(e) || (t = C.propFix[t] || t, 
            r = C.propHooks[t]), void 0 !== n ? r && "set" in r && void 0 !== (i = r.set(e, n, t)) ? i : e[t] = n : r && "get" in r && null !== (i = r.get(e, t)) ? i : e[t];
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = C.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : gt.test(e.nodeName) || vt.test(e.nodeName) && e.href ? 0 : -1;
                }
            }
        },
        propFix: {
            for: "htmlFor",
            class: "className"
        }
    }), y.optSelected || (C.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null;
        },
        set: function(e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex);
        }
    }), C.each([ "tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable" ], function() {
        C.propFix[this.toLowerCase()] = this;
    });
    function yt(e) {
        return (e.match(L) || []).join(" ");
    }
    function mt(e) {
        return e.getAttribute && e.getAttribute("class") || "";
    }
    function xt(e) {
        return Array.isArray(e) ? e : "string" == typeof e ? e.match(L) || [] : [];
    }
    C.fn.extend({
        addClass: function(t) {
            var e, n, i, r, o, s, a, l = 0;
            if (m(t)) return this.each(function(e) {
                C(this).addClass(t.call(this, e, mt(this)));
            });
            if ((e = xt(t)).length) while (n = this[l++]) if (r = mt(n), i = 1 === n.nodeType && " " + yt(r) + " ") {
                s = 0;
                while (o = e[s++]) i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                r !== (a = yt(i)) && n.setAttribute("class", a);
            }
            return this;
        },
        removeClass: function(t) {
            var e, n, i, r, o, s, a, l = 0;
            if (m(t)) return this.each(function(e) {
                C(this).removeClass(t.call(this, e, mt(this)));
            });
            if (!arguments.length) return this.attr("class", "");
            if ((e = xt(t)).length) while (n = this[l++]) if (r = mt(n), i = 1 === n.nodeType && " " + yt(r) + " ") {
                s = 0;
                while (o = e[s++]) while (i.indexOf(" " + o + " ") > -1) i = i.replace(" " + o + " ", " ");
                r !== (a = yt(i)) && n.setAttribute("class", a);
            }
            return this;
        },
        toggleClass: function(r, t) {
            var o = typeof r, s = "string" === o || Array.isArray(r);
            return "boolean" == typeof t && s ? t ? this.addClass(r) : this.removeClass(r) : m(r) ? this.each(function(e) {
                C(this).toggleClass(r.call(this, e, mt(this), t), t);
            }) : this.each(function() {
                var e, t, n, i;
                if (s) {
                    t = 0, n = C(this), i = xt(r);
                    while (e = i[t++]) n.hasClass(e) ? n.removeClass(e) : n.addClass(e);
                } else void 0 !== r && "boolean" !== o || ((e = mt(this)) && Y.set(this, "__className__", e), 
                this.setAttribute && this.setAttribute("class", e || !1 === r ? "" : Y.get(this, "__className__") || ""));
            });
        },
        hasClass: function(e) {
            var t, n, i = 0;
            t = " " + e + " ";
            while (n = this[i++]) if (1 === n.nodeType && (" " + yt(mt(n)) + " ").indexOf(t) > -1) return !0;
            return !1;
        }
    });
    var bt = /\r/g;
    C.fn.extend({
        val: function(n) {
            var i, e, r, t = this[0];
            {
                if (arguments.length) return r = m(n), this.each(function(e) {
                    var t;
                    1 === this.nodeType && (null == (t = r ? n.call(this, e, C(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = C.map(t, function(e) {
                        return null == e ? "" : e + "";
                    })), (i = C.valHooks[this.type] || C.valHooks[this.nodeName.toLowerCase()]) && "set" in i && void 0 !== i.set(this, t, "value") || (this.value = t));
                });
                if (t) return (i = C.valHooks[t.type] || C.valHooks[t.nodeName.toLowerCase()]) && "get" in i && void 0 !== (e = i.get(t, "value")) ? e : "string" == typeof (e = t.value) ? e.replace(bt, "") : null == e ? "" : e;
            }
        }
    }), C.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = C.find.attr(e, "value");
                    return null != t ? t : yt(C.text(e));
                }
            },
            select: {
                get: function(e) {
                    var t, n, i, r = e.options, o = e.selectedIndex, s = "select-one" === e.type, a = s ? null : [], l = s ? o + 1 : r.length;
                    for (i = o < 0 ? l : s ? o : 0; i < l; i++) if (((n = r[i]).selected || i === o) && !n.disabled && (!n.parentNode.disabled || !k(n.parentNode, "optgroup"))) {
                        if (t = C(n).val(), s) return t;
                        a.push(t);
                    }
                    return a;
                },
                set: function(e, t) {
                    var n, i, r = e.options, o = C.makeArray(t), s = r.length;
                    while (s--) ((i = r[s]).selected = C.inArray(C.valHooks.option.get(i), o) > -1) && (n = !0);
                    return n || (e.selectedIndex = -1), o;
                }
            }
        }
    }), C.each([ "radio", "checkbox" ], function() {
        C.valHooks[this] = {
            set: function(e, t) {
                if (Array.isArray(t)) return e.checked = C.inArray(C(e).val(), t) > -1;
            }
        }, y.checkOn || (C.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value;
        });
    }), y.focusin = "onfocusin" in A;
    var wt = /^(?:focusinfocus|focusoutblur)$/, St = function(e) {
        e.stopPropagation();
    };
    C.extend(C.event, {
        trigger: function(e, t, n, i) {
            var r, o, s, a, l, c, u, d, f = [ n || T ], p = v.call(e, "type") ? e.type : e, h = v.call(e, "namespace") ? e.namespace.split(".") : [];
            if (o = d = s = n = n || T, 3 !== n.nodeType && 8 !== n.nodeType && !wt.test(p + C.event.triggered) && (p.indexOf(".") > -1 && (p = (h = p.split(".")).shift(), 
            h.sort()), l = p.indexOf(":") < 0 && "on" + p, e = e[C.expando] ? e : new C.Event(p, "object" == typeof e && e), 
            e.isTrigger = i ? 2 : 3, e.namespace = h.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, 
            e.result = void 0, e.target || (e.target = n), t = null == t ? [ e ] : C.makeArray(t, [ e ]), 
            u = C.event.special[p] || {}, i || !u.trigger || !1 !== u.trigger.apply(n, t))) {
                if (!i && !u.noBubble && !x(n)) {
                    for (a = u.delegateType || p, wt.test(a + p) || (o = o.parentNode); o; o = o.parentNode) f.push(o), 
                    s = o;
                    s === (n.ownerDocument || T) && f.push(s.defaultView || s.parentWindow || A);
                }
                r = 0;
                while ((o = f[r++]) && !e.isPropagationStopped()) d = o, e.type = r > 1 ? a : u.bindType || p, 
                (c = (Y.get(o, "events") || {})[e.type] && Y.get(o, "handle")) && c.apply(o, t), 
                (c = l && o[l]) && c.apply && Z(o) && (e.result = c.apply(o, t), !1 === e.result && e.preventDefault());
                return e.type = p, i || e.isDefaultPrevented() || u._default && !1 !== u._default.apply(f.pop(), t) || !Z(n) || l && m(n[p]) && !x(n) && ((s = n[l]) && (n[l] = null), 
                C.event.triggered = p, e.isPropagationStopped() && d.addEventListener(p, St), n[p](), 
                e.isPropagationStopped() && d.removeEventListener(p, St), C.event.triggered = void 0, 
                s && (n[l] = s)), e.result;
            }
        },
        simulate: function(e, t, n) {
            var i = C.extend(new C.Event(), n, {
                type: e,
                isSimulated: !0
            });
            C.event.trigger(i, null, t);
        }
    }), C.fn.extend({
        trigger: function(e, t) {
            return this.each(function() {
                C.event.trigger(e, t, this);
            });
        },
        triggerHandler: function(e, t) {
            var n = this[0];
            if (n) return C.event.trigger(e, t, n, !0);
        }
    }), y.focusin || C.each({
        focus: "focusin",
        blur: "focusout"
    }, function(n, i) {
        var r = function(e) {
            C.event.simulate(i, e.target, C.event.fix(e));
        };
        C.event.special[i] = {
            setup: function() {
                var e = this.ownerDocument || this, t = Y.access(e, i);
                t || e.addEventListener(n, r, !0), Y.access(e, i, (t || 0) + 1);
            },
            teardown: function() {
                var e = this.ownerDocument || this, t = Y.access(e, i) - 1;
                t ? Y.access(e, i, t) : (e.removeEventListener(n, r, !0), Y.remove(e, i));
            }
        };
    });
    var At = A.location, Tt = Date.now(), Ct = /\?/;
    C.parseXML = function(e) {
        var t;
        if (!e || "string" != typeof e) return null;
        try {
            t = new A.DOMParser().parseFromString(e, "text/xml");
        } catch (e) {
            t = void 0;
        }
        return t && !t.getElementsByTagName("parsererror").length || C.error("Invalid XML: " + e), 
        t;
    };
    var It = /\[\]$/, Pt = /\r?\n/g, kt = /^(?:submit|button|image|reset|file)$/i, Et = /^(?:input|select|textarea|keygen)/i;
    function Dt(n, e, i, r) {
        var t;
        if (Array.isArray(e)) C.each(e, function(e, t) {
            i || It.test(n) ? r(n, t) : Dt(n + "[" + ("object" == typeof t && null != t ? e : "") + "]", t, i, r);
        }); else if (i || "object" !== w(e)) r(n, e); else for (t in e) Dt(n + "[" + t + "]", e[t], i, r);
    }
    C.param = function(e, t) {
        var n, i = [], r = function(e, t) {
            var n = m(t) ? t() : t;
            i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n);
        };
        if (Array.isArray(e) || e.jquery && !C.isPlainObject(e)) C.each(e, function() {
            r(this.name, this.value);
        }); else for (n in e) Dt(n, e[n], t, r);
        return i.join("&");
    }, C.fn.extend({
        serialize: function() {
            return C.param(this.serializeArray());
        },
        serializeArray: function() {
            return this.map(function() {
                var e = C.prop(this, "elements");
                return e ? C.makeArray(e) : this;
            }).filter(function() {
                var e = this.type;
                return this.name && !C(this).is(":disabled") && Et.test(this.nodeName) && !kt.test(e) && (this.checked || !fe.test(e));
            }).map(function(e, t) {
                var n = C(this).val();
                return null == n ? null : Array.isArray(n) ? C.map(n, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(Pt, "\r\n")
                    };
                }) : {
                    name: t.name,
                    value: n.replace(Pt, "\r\n")
                };
            }).get();
        }
    });
    var Nt = /%20/g, jt = /#.*$/, qt = /([?&])_=[^&]*/, Ht = /^(.*?):[ \t]*([^\r\n]*)$/gm, Ot = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, Lt = /^(?:GET|HEAD)$/, _t = /^\/\//, Rt = {}, Wt = {}, Ft = "*/".concat("*"), Mt = T.createElement("a");
    Mt.href = At.href;
    function $t(o) {
        return function(e, t) {
            "string" != typeof e && (t = e, e = "*");
            var n, i = 0, r = e.toLowerCase().match(L) || [];
            if (m(t)) while (n = r[i++]) "+" === n[0] ? (n = n.slice(1) || "*", (o[n] = o[n] || []).unshift(t)) : (o[n] = o[n] || []).push(t);
        };
    }
    function zt(t, r, o, s) {
        var a = {}, l = t === Wt;
        function c(e) {
            var i;
            return a[e] = !0, C.each(t[e] || [], function(e, t) {
                var n = t(r, o, s);
                return "string" != typeof n || l || a[n] ? l ? !(i = n) : void 0 : (r.dataTypes.unshift(n), 
                c(n), !1);
            }), i;
        }
        return c(r.dataTypes[0]) || !a["*"] && c("*");
    }
    function Bt(e, t) {
        var n, i, r = C.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((r[n] ? e : i || (i = {}))[n] = t[n]);
        return i && C.extend(!0, e, i), e;
    }
    function Vt(e, t, n) {
        var i, r, o, s, a = e.contents, l = e.dataTypes;
        while ("*" === l[0]) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
        if (i) for (r in a) if (a[r] && a[r].test(i)) {
            l.unshift(r);
            break;
        }
        if (l[0] in n) o = l[0]; else {
            for (r in n) {
                if (!l[0] || e.converters[r + " " + l[0]]) {
                    o = r;
                    break;
                }
                s || (s = r);
            }
            o = o || s;
        }
        if (o) return o !== l[0] && l.unshift(o), n[o];
    }
    function Ut(e, t, n, i) {
        var r, o, s, a, l, c = {}, u = e.dataTypes.slice();
        if (u[1]) for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
        o = u.shift();
        while (o) if (e.responseFields[o] && (n[e.responseFields[o]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), 
        l = o, o = u.shift()) if ("*" === o) o = l; else if ("*" !== l && l !== o) {
            if (!(s = c[l + " " + o] || c["* " + o])) for (r in c) if ((a = r.split(" "))[1] === o && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                !0 === s ? s = c[r] : !0 !== c[r] && (o = a[0], u.unshift(a[1]));
                break;
            }
            if (!0 !== s) if (s && e["throws"]) t = s(t); else try {
                t = s(t);
            } catch (e) {
                return {
                    state: "parsererror",
                    error: s ? e : "No conversion from " + l + " to " + o
                };
            }
        }
        return {
            state: "success",
            data: t
        };
    }
    C.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: At.href,
            type: "GET",
            isLocal: Ot.test(At.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Ft,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": C.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? Bt(Bt(e, C.ajaxSettings), t) : Bt(C.ajaxSettings, e);
        },
        ajaxPrefilter: $t(Rt),
        ajaxTransport: $t(Wt),
        ajax: function(e, t) {
            "object" == typeof e && (t = e, e = void 0), t = t || {};
            var u, d, f, n, p, i, h, g, r, o, v = C.ajaxSetup({}, t), y = v.context || v, m = v.context && (y.nodeType || y.jquery) ? C(y) : C.event, x = C.Deferred(), b = C.Callbacks("once memory"), w = v.statusCode || {}, s = {}, a = {}, l = "canceled", S = {
                readyState: 0,
                getResponseHeader: function(e) {
                    var t;
                    if (h) {
                        if (!n) {
                            n = {};
                            while (t = Ht.exec(f)) n[t[1].toLowerCase()] = t[2];
                        }
                        t = n[e.toLowerCase()];
                    }
                    return null == t ? null : t;
                },
                getAllResponseHeaders: function() {
                    return h ? f : null;
                },
                setRequestHeader: function(e, t) {
                    return null == h && (e = a[e.toLowerCase()] = a[e.toLowerCase()] || e, s[e] = t), 
                    this;
                },
                overrideMimeType: function(e) {
                    return null == h && (v.mimeType = e), this;
                },
                statusCode: function(e) {
                    var t;
                    if (e) if (h) S.always(e[S.status]); else for (t in e) w[t] = [ w[t], e[t] ];
                    return this;
                },
                abort: function(e) {
                    var t = e || l;
                    return u && u.abort(t), c(0, t), this;
                }
            };
            if (x.promise(S), v.url = ((e || v.url || At.href) + "").replace(_t, At.protocol + "//"), 
            v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(L) || [ "" ], 
            null == v.crossDomain) {
                i = T.createElement("a");
                try {
                    i.href = v.url, i.href = i.href, v.crossDomain = Mt.protocol + "//" + Mt.host != i.protocol + "//" + i.host;
                } catch (e) {
                    v.crossDomain = !0;
                }
            }
            if (v.data && v.processData && "string" != typeof v.data && (v.data = C.param(v.data, v.traditional)), 
            zt(Rt, v, t, S), h) return S;
            (g = C.event && v.global) && 0 == C.active++ && C.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), 
            v.hasContent = !Lt.test(v.type), d = v.url.replace(jt, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(Nt, "+")) : (o = v.url.slice(d.length), 
            v.data && (v.processData || "string" == typeof v.data) && (d += (Ct.test(d) ? "&" : "?") + v.data, 
            delete v.data), !1 === v.cache && (d = d.replace(qt, "$1"), o = (Ct.test(d) ? "&" : "?") + "_=" + Tt++ + o), 
            v.url = d + o), v.ifModified && (C.lastModified[d] && S.setRequestHeader("If-Modified-Since", C.lastModified[d]), 
            C.etag[d] && S.setRequestHeader("If-None-Match", C.etag[d])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && S.setRequestHeader("Content-Type", v.contentType), 
            S.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + Ft + "; q=0.01" : "") : v.accepts["*"]);
            for (r in v.headers) S.setRequestHeader(r, v.headers[r]);
            if (v.beforeSend && (!1 === v.beforeSend.call(y, S, v) || h)) return S.abort();
            if (l = "abort", b.add(v.complete), S.done(v.success), S.fail(v.error), u = zt(Wt, v, t, S)) {
                if (S.readyState = 1, g && m.trigger("ajaxSend", [ S, v ]), h) return S;
                v.async && v.timeout > 0 && (p = A.setTimeout(function() {
                    S.abort("timeout");
                }, v.timeout));
                try {
                    h = !1, u.send(s, c);
                } catch (e) {
                    if (h) throw e;
                    c(-1, e);
                }
            } else c(-1, "No Transport");
            function c(e, t, n, i) {
                var r, o, s, a, l, c = t;
                h || (h = !0, p && A.clearTimeout(p), u = void 0, f = i || "", S.readyState = e > 0 ? 4 : 0, 
                r = e >= 200 && e < 300 || 304 === e, n && (a = Vt(v, S, n)), a = Ut(v, a, S, r), 
                r ? (v.ifModified && ((l = S.getResponseHeader("Last-Modified")) && (C.lastModified[d] = l), 
                (l = S.getResponseHeader("etag")) && (C.etag[d] = l)), 204 === e || "HEAD" === v.type ? c = "nocontent" : 304 === e ? c = "notmodified" : (c = a.state, 
                o = a.data, r = !(s = a.error))) : (s = c, !e && c || (c = "error", e < 0 && (e = 0))), 
                S.status = e, S.statusText = (t || c) + "", r ? x.resolveWith(y, [ o, c, S ]) : x.rejectWith(y, [ S, c, s ]), 
                S.statusCode(w), w = void 0, g && m.trigger(r ? "ajaxSuccess" : "ajaxError", [ S, v, r ? o : s ]), 
                b.fireWith(y, [ S, c ]), g && (m.trigger("ajaxComplete", [ S, v ]), --C.active || C.event.trigger("ajaxStop")));
            }
            return S;
        },
        getJSON: function(e, t, n) {
            return C.get(e, t, n, "json");
        },
        getScript: function(e, t) {
            return C.get(e, void 0, t, "script");
        }
    }), C.each([ "get", "post" ], function(e, r) {
        C[r] = function(e, t, n, i) {
            return m(t) && (i = i || n, n = t, t = void 0), C.ajax(C.extend({
                url: e,
                type: r,
                dataType: i,
                data: t,
                success: n
            }, C.isPlainObject(e) && e));
        };
    }), C._evalUrl = function(e) {
        return C.ajax({
            url: e,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            throws: !0
        });
    }, C.fn.extend({
        wrapAll: function(e) {
            var t;
            return this[0] && (m(e) && (e = e.call(this[0])), t = C(e, this[0].ownerDocument).eq(0).clone(!0), 
            this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                var e = this;
                while (e.firstElementChild) e = e.firstElementChild;
                return e;
            }).append(this)), this;
        },
        wrapInner: function(n) {
            return m(n) ? this.each(function(e) {
                C(this).wrapInner(n.call(this, e));
            }) : this.each(function() {
                var e = C(this), t = e.contents();
                t.length ? t.wrapAll(n) : e.append(n);
            });
        },
        wrap: function(t) {
            var n = m(t);
            return this.each(function(e) {
                C(this).wrapAll(n ? t.call(this, e) : t);
            });
        },
        unwrap: function(e) {
            return this.parent(e).not("body").each(function() {
                C(this).replaceWith(this.childNodes);
            }), this;
        }
    }), C.expr.pseudos.hidden = function(e) {
        return !C.expr.pseudos.visible(e);
    }, C.expr.pseudos.visible = function(e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length);
    }, C.ajaxSettings.xhr = function() {
        try {
            return new A.XMLHttpRequest();
        } catch (e) {}
    };
    var Xt = {
        0: 200,
        1223: 204
    }, Qt = C.ajaxSettings.xhr();
    y.cors = !!Qt && "withCredentials" in Qt, y.ajax = Qt = !!Qt, C.ajaxTransport(function(r) {
        var o, s;
        if (y.cors || Qt && !r.crossDomain) return {
            send: function(e, t) {
                var n, i = r.xhr();
                if (i.open(r.type, r.url, r.async, r.username, r.password), r.xhrFields) for (n in r.xhrFields) i[n] = r.xhrFields[n];
                r.mimeType && i.overrideMimeType && i.overrideMimeType(r.mimeType), r.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest");
                for (n in e) i.setRequestHeader(n, e[n]);
                o = function(e) {
                    return function() {
                        o && (o = s = i.onload = i.onerror = i.onabort = i.ontimeout = i.onreadystatechange = null, 
                        "abort" === e ? i.abort() : "error" === e ? "number" != typeof i.status ? t(0, "error") : t(i.status, i.statusText) : t(Xt[i.status] || i.status, i.statusText, "text" !== (i.responseType || "text") || "string" != typeof i.responseText ? {
                            binary: i.response
                        } : {
                            text: i.responseText
                        }, i.getAllResponseHeaders()));
                    };
                }, i.onload = o(), s = i.onerror = i.ontimeout = o("error"), void 0 !== i.onabort ? i.onabort = s : i.onreadystatechange = function() {
                    4 === i.readyState && A.setTimeout(function() {
                        o && s();
                    });
                }, o = o("abort");
                try {
                    i.send(r.hasContent && r.data || null);
                } catch (e) {
                    if (o) throw e;
                }
            },
            abort: function() {
                o && o();
            }
        };
    }), C.ajaxPrefilter(function(e) {
        e.crossDomain && (e.contents.script = !1);
    }), C.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(e) {
                return C.globalEval(e), e;
            }
        }
    }), C.ajaxPrefilter("script", function(e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET");
    }), C.ajaxTransport("script", function(n) {
        if (n.crossDomain) {
            var i, r;
            return {
                send: function(e, t) {
                    i = C("<script>").prop({
                        charset: n.scriptCharset,
                        src: n.url
                    }).on("load error", r = function(e) {
                        i.remove(), r = null, e && t("error" === e.type ? 404 : 200, e.type);
                    }), T.head.appendChild(i[0]);
                },
                abort: function() {
                    r && r();
                }
            };
        }
    });
    var Zt = [], Gt = /(=)\?(?=&|$)|\?\?/;
    C.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = Zt.pop() || C.expando + "_" + Tt++;
            return this[e] = !0, e;
        }
    }), C.ajaxPrefilter("json jsonp", function(e, t, n) {
        var i, r, o, s = !1 !== e.jsonp && (Gt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Gt.test(e.data) && "data");
        if (s || "jsonp" === e.dataTypes[0]) return i = e.jsonpCallback = m(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, 
        s ? e[s] = e[s].replace(Gt, "$1" + i) : !1 !== e.jsonp && (e.url += (Ct.test(e.url) ? "&" : "?") + e.jsonp + "=" + i), 
        e.converters["script json"] = function() {
            return o || C.error(i + " was not called"), o[0];
        }, e.dataTypes[0] = "json", r = A[i], A[i] = function() {
            o = arguments;
        }, n.always(function() {
            void 0 === r ? C(A).removeProp(i) : A[i] = r, e[i] && (e.jsonpCallback = t.jsonpCallback, 
            Zt.push(i)), o && m(r) && r(o[0]), o = r = void 0;
        }), "script";
    }), y.createHTMLDocument = function() {
        var e = T.implementation.createHTMLDocument("").body;
        return e.innerHTML = "<form></form><form></form>", 2 === e.childNodes.length;
    }(), C.parseHTML = function(e, t, n) {
        if ("string" != typeof e) return [];
        "boolean" == typeof t && (n = t, t = !1);
        var i, r, o;
        return t || (y.createHTMLDocument ? ((i = (t = T.implementation.createHTMLDocument("")).createElement("base")).href = T.location.href, 
        t.head.appendChild(i)) : t = T), r = E.exec(e), o = !n && [], r ? [ t.createElement(r[1]) ] : (r = xe([ e ], t, o), 
        o && o.length && C(o).remove(), C.merge([], r.childNodes));
    }, C.fn.load = function(e, t, n) {
        var i, r, o, s = this, a = e.indexOf(" ");
        return a > -1 && (i = yt(e.slice(a)), e = e.slice(0, a)), m(t) ? (n = t, t = void 0) : t && "object" == typeof t && (r = "POST"), 
        s.length > 0 && C.ajax({
            url: e,
            type: r || "GET",
            dataType: "html",
            data: t
        }).done(function(e) {
            o = arguments, s.html(i ? C("<div>").append(C.parseHTML(e)).find(i) : e);
        }).always(n && function(e, t) {
            s.each(function() {
                n.apply(this, o || [ e.responseText, t, e ]);
            });
        }), this;
    }, C.each([ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function(e, t) {
        C.fn[t] = function(e) {
            return this.on(t, e);
        };
    }), C.expr.pseudos.animated = function(t) {
        return C.grep(C.timers, function(e) {
            return t === e.elem;
        }).length;
    }, C.offset = {
        setOffset: function(e, t, n) {
            var i, r, o, s, a, l, c, u = C.css(e, "position"), d = C(e), f = {};
            "static" === u && (e.style.position = "relative"), a = d.offset(), o = C.css(e, "top"), 
            l = C.css(e, "left"), (c = ("absolute" === u || "fixed" === u) && (o + l).indexOf("auto") > -1) ? (s = (i = d.position()).top, 
            r = i.left) : (s = parseFloat(o) || 0, r = parseFloat(l) || 0), m(t) && (t = t.call(e, n, C.extend({}, a))), 
            null != t.top && (f.top = t.top - a.top + s), null != t.left && (f.left = t.left - a.left + r), 
            "using" in t ? t.using.call(e, f) : d.css(f);
        }
    }, C.fn.extend({
        offset: function(t) {
            if (arguments.length) return void 0 === t ? this : this.each(function(e) {
                C.offset.setOffset(this, t, e);
            });
            var e, n, i = this[0];
            if (i) return i.getClientRects().length ? (e = i.getBoundingClientRect(), n = i.ownerDocument.defaultView, 
            {
                top: e.top + n.pageYOffset,
                left: e.left + n.pageXOffset
            }) : {
                top: 0,
                left: 0
            };
        },
        position: function() {
            if (this[0]) {
                var e, t, n, i = this[0], r = {
                    top: 0,
                    left: 0
                };
                if ("fixed" === C.css(i, "position")) t = i.getBoundingClientRect(); else {
                    t = this.offset(), n = i.ownerDocument, e = i.offsetParent || n.documentElement;
                    while (e && (e === n.body || e === n.documentElement) && "static" === C.css(e, "position")) e = e.parentNode;
                    e && e !== i && 1 === e.nodeType && ((r = C(e).offset()).top += C.css(e, "borderTopWidth", !0), 
                    r.left += C.css(e, "borderLeftWidth", !0));
                }
                return {
                    top: t.top - r.top - C.css(i, "marginTop", !0),
                    left: t.left - r.left - C.css(i, "marginLeft", !0)
                };
            }
        },
        offsetParent: function() {
            return this.map(function() {
                var e = this.offsetParent;
                while (e && "static" === C.css(e, "position")) e = e.offsetParent;
                return e || be;
            });
        }
    }), C.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(t, r) {
        var o = "pageYOffset" === r;
        C.fn[t] = function(e) {
            return B(this, function(e, t, n) {
                var i;
                if (x(e) ? i = e : 9 === e.nodeType && (i = e.defaultView), void 0 === n) return i ? i[r] : e[t];
                i ? i.scrollTo(o ? i.pageXOffset : n, o ? n : i.pageYOffset) : e[t] = n;
            }, t, e, arguments.length);
        };
    }), C.each([ "top", "left" ], function(e, n) {
        C.cssHooks[n] = ze(y.pixelPosition, function(e, t) {
            if (t) return t = $e(e, n), We.test(t) ? C(e).position()[n] + "px" : t;
        });
    }), C.each({
        Height: "height",
        Width: "width"
    }, function(s, a) {
        C.each({
            padding: "inner" + s,
            content: a,
            "": "outer" + s
        }, function(i, o) {
            C.fn[o] = function(e, t) {
                var n = arguments.length && (i || "boolean" != typeof e), r = i || (!0 === e || !0 === t ? "margin" : "border");
                return B(this, function(e, t, n) {
                    var i;
                    return x(e) ? 0 === o.indexOf("outer") ? e["inner" + s] : e.document.documentElement["client" + s] : 9 === e.nodeType ? (i = e.documentElement, 
                    Math.max(e.body["scroll" + s], i["scroll" + s], e.body["offset" + s], i["offset" + s], i["client" + s])) : void 0 === n ? C.css(e, t, r) : C.style(e, t, n, r);
                }, a, n ? e : void 0, n);
            };
        });
    }), C.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(e, n) {
        C.fn[n] = function(e, t) {
            return arguments.length > 0 ? this.on(n, null, e, t) : this.trigger(n);
        };
    }), C.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e);
        }
    }), C.fn.extend({
        bind: function(e, t, n) {
            return this.on(e, null, t, n);
        },
        unbind: function(e, t) {
            return this.off(e, null, t);
        },
        delegate: function(e, t, n, i) {
            return this.on(t, e, n, i);
        },
        undelegate: function(e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n);
        }
    }), C.proxy = function(e, t) {
        var n, i, r;
        if ("string" == typeof t && (n = e[t], t = e, e = n), m(e)) return i = a.call(arguments, 2), 
        r = function() {
            return e.apply(t || this, i.concat(a.call(arguments)));
        }, r.guid = e.guid = e.guid || C.guid++, r;
    }, C.holdReady = function(e) {
        e ? C.readyWait++ : C.ready(!0);
    }, C.isArray = Array.isArray, C.parseJSON = JSON.parse, C.nodeName = k, C.isFunction = m, 
    C.isWindow = x, C.camelCase = Q, C.type = w, C.now = Date.now, C.isNumeric = function(e) {
        var t = C.type(e);
        return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e));
    }, "function" == typeof define && define.amd && define("jquery", [], function() {
        return C;
    });
    var Yt = A.jQuery, Jt = A.$;
    return C.noConflict = function(e) {
        return A.$ === C && (A.$ = Jt), e && A.jQuery === C && (A.jQuery = Yt), C;
    }, e || (A.jQuery = A.$ = C), C;
});

!function(c) {
    "use strict";
    function l(e) {
        return (e || "").toLowerCase();
    }
    var e = "2.1.6";
    c.fn.cycle = function(a) {
        var e;
        return 0 !== this.length || c.isReady ? this.each(function() {
            var e, n, t, i, r = c(this), o = c.fn.cycle.log;
            if (!r.data("cycle.opts")) {
                (r.data("cycle-log") === !1 || a && a.log === !1 || n && n.log === !1) && (o = c.noop), 
                o("--c2 init--"), e = r.data();
                for (var s in e) e.hasOwnProperty(s) && /^cycle[A-Z]+/.test(s) && (i = e[s], t = s.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, l), 
                o(t + ":", i, "(" + typeof i + ")"), e[t] = i);
                n = c.extend({}, c.fn.cycle.defaults, e, a || {}), n.timeoutId = 0, n.paused = n.paused || !1, 
                n.container = r, n._maxZ = n.maxZ, n.API = c.extend({
                    _container: r
                }, c.fn.cycle.API), n.API.log = o, n.API.trigger = function(e, t) {
                    return n.container.trigger(e, t), n.API;
                }, r.data("cycle.opts", n), r.data("cycle.API", n.API), n.API.trigger("cycle-bootstrap", [ n, n.API ]), 
                n.API.addInitialSlides(), n.API.preInitSlideshow(), n.slides.length && n.API.initSlideshow();
            }
        }) : (e = {
            s: this.selector,
            c: this.context
        }, c.fn.cycle.log("requeuing slideshow (dom not ready)"), c(function() {
            c(e.s, e.c).cycle(a);
        }), this);
    }, c.fn.cycle.API = {
        opts: function() {
            return this._container.data("cycle.opts");
        },
        addInitialSlides: function() {
            var e = this.opts(), t = e.slides;
            e.slideCount = 0, e.slides = c(), t = t.jquery ? t : e.container.find(t), e.random && t.sort(function() {
                return Math.random() - .5;
            }), e.API.add(t);
        },
        preInitSlideshow: function() {
            var e = this.opts();
            e.API.trigger("cycle-pre-initialize", [ e ]);
            var t = c.fn.cycle.transitions[e.fx];
            t && c.isFunction(t.preInit) && t.preInit(e), e._preInitialized = !0;
        },
        postInitSlideshow: function() {
            var e = this.opts();
            e.API.trigger("cycle-post-initialize", [ e ]);
            var t = c.fn.cycle.transitions[e.fx];
            t && c.isFunction(t.postInit) && t.postInit(e);
        },
        initSlideshow: function() {
            var e, t = this.opts(), n = t.container;
            t.API.calcFirstSlide(), "static" == t.container.css("position") && t.container.css("position", "relative"), 
            c(t.slides[t.currSlide]).css({
                opacity: 1,
                display: "block",
                visibility: "visible"
            }), t.API.stackSlides(t.slides[t.currSlide], t.slides[t.nextSlide], !t.reverse), 
            t.pauseOnHover && (t.pauseOnHover !== !0 && (n = c(t.pauseOnHover)), n.hover(function() {
                t.API.pause(!0);
            }, function() {
                t.API.resume(!0);
            })), t.timeout && (e = t.API.getSlideOpts(t.currSlide), t.API.queueTransition(e, e.timeout + t.delay)), 
            t._initialized = !0, t.API.updateView(!0), t.API.trigger("cycle-initialized", [ t ]), 
            t.API.postInitSlideshow();
        },
        pause: function(e) {
            var t = this.opts(), n = t.API.getSlideOpts(), i = t.hoverPaused || t.paused;
            e ? t.hoverPaused = !0 : t.paused = !0, i || (t.container.addClass("cycle-paused"), 
            t.API.trigger("cycle-paused", [ t ]).log("cycle-paused"), n.timeout && (clearTimeout(t.timeoutId), 
            t.timeoutId = 0, t._remainingTimeout -= c.now() - t._lastQueue, (t._remainingTimeout < 0 || isNaN(t._remainingTimeout)) && (t._remainingTimeout = void 0)));
        },
        resume: function(e) {
            var t = this.opts(), n = !t.hoverPaused && !t.paused;
            e ? t.hoverPaused = !1 : t.paused = !1, n || (t.container.removeClass("cycle-paused"), 
            0 === t.slides.filter(":animated").length && t.API.queueTransition(t.API.getSlideOpts(), t._remainingTimeout), 
            t.API.trigger("cycle-resumed", [ t, t._remainingTimeout ]).log("cycle-resumed"));
        },
        add: function(e, n) {
            var t, i = this.opts(), r = i.slideCount, o = !1;
            "string" == c.type(e) && (e = c.trim(e)), c(e).each(function() {
                var e, t = c(this);
                n ? i.container.prepend(t) : i.container.append(t), i.slideCount++, e = i.API.buildSlideOpts(t), 
                i.slides = n ? c(t).add(i.slides) : i.slides.add(t), i.API.initSlide(e, t, --i._maxZ), 
                t.data("cycle.opts", e), i.API.trigger("cycle-slide-added", [ i, e, t ]);
            }), i.API.updateView(!0), o = i._preInitialized && 2 > r && i.slideCount >= 1, o && (i._initialized ? i.timeout && (t = i.slides.length, 
            i.nextSlide = i.reverse ? t - 1 : 1, i.timeoutId || i.API.queueTransition(i)) : i.API.initSlideshow());
        },
        calcFirstSlide: function() {
            var e, t = this.opts();
            e = parseInt(t.startingSlide || 0, 10), (e >= t.slides.length || 0 > e) && (e = 0), 
            t.currSlide = e, t.reverse ? (t.nextSlide = e - 1, t.nextSlide < 0 && (t.nextSlide = t.slides.length - 1)) : (t.nextSlide = e + 1, 
            t.nextSlide == t.slides.length && (t.nextSlide = 0));
        },
        calcNextSlide: function() {
            var e, t = this.opts();
            t.reverse ? (e = t.nextSlide - 1 < 0, t.nextSlide = e ? t.slideCount - 1 : t.nextSlide - 1, 
            t.currSlide = e ? 0 : t.nextSlide + 1) : (e = t.nextSlide + 1 == t.slides.length, 
            t.nextSlide = e ? 0 : t.nextSlide + 1, t.currSlide = e ? t.slides.length - 1 : t.nextSlide - 1);
        },
        calcTx: function(e, t) {
            var n, i = e;
            return i._tempFx ? n = c.fn.cycle.transitions[i._tempFx] : t && i.manualFx && (n = c.fn.cycle.transitions[i.manualFx]), 
            n || (n = c.fn.cycle.transitions[i.fx]), i._tempFx = null, this.opts()._tempFx = null, 
            n || (n = c.fn.cycle.transitions.fade, i.API.log('Transition "' + i.fx + '" not found.  Using fade.')), 
            n;
        },
        prepareTx: function(e, t) {
            var n, i, r, o, s, a = this.opts();
            return a.slideCount < 2 ? void (a.timeoutId = 0) : (!e || a.busy && !a.manualTrump || (a.API.stopTransition(), 
            a.busy = !1, clearTimeout(a.timeoutId), a.timeoutId = 0), void (a.busy || (0 !== a.timeoutId || e) && (i = a.slides[a.currSlide], 
            r = a.slides[a.nextSlide], o = a.API.getSlideOpts(a.nextSlide), s = a.API.calcTx(o, e), 
            a._tx = s, e && void 0 !== o.manualSpeed && (o.speed = o.manualSpeed), a.nextSlide != a.currSlide && (e || !a.paused && !a.hoverPaused && a.timeout) ? (a.API.trigger("cycle-before", [ o, i, r, t ]), 
            s.before && s.before(o, i, r, t), n = function() {
                a.busy = !1, a.container.data("cycle.opts") && (s.after && s.after(o, i, r, t), 
                a.API.trigger("cycle-after", [ o, i, r, t ]), a.API.queueTransition(o), a.API.updateView(!0));
            }, a.busy = !0, s.transition ? s.transition(o, i, r, t, n) : a.API.doTransition(o, i, r, t, n), 
            a.API.calcNextSlide(), a.API.updateView()) : a.API.queueTransition(o))));
        },
        doTransition: function(e, t, n, i, r) {
            var o = e, s = c(t), a = c(n), l = function() {
                a.animate(o.animIn || {
                    opacity: 1
                }, o.speed, o.easeIn || o.easing, r);
            };
            a.css(o.cssBefore || {}), s.animate(o.animOut || {}, o.speed, o.easeOut || o.easing, function() {
                s.css(o.cssAfter || {}), o.sync || l();
            }), o.sync && l();
        },
        queueTransition: function(e, t) {
            var n = this.opts(), i = void 0 !== t ? t : e.timeout;
            return 0 === n.nextSlide && 0 === --n.loop ? (n.API.log("terminating; loop=0"), 
            n.timeout = 0, i ? setTimeout(function() {
                n.API.trigger("cycle-finished", [ n ]);
            }, i) : n.API.trigger("cycle-finished", [ n ]), void (n.nextSlide = n.currSlide)) : void 0 !== n.continueAuto && (n.continueAuto === !1 || c.isFunction(n.continueAuto) && n.continueAuto() === !1) ? (n.API.log("terminating automatic transitions"), 
            n.timeout = 0, void (n.timeoutId && clearTimeout(n.timeoutId))) : void (i && (n._lastQueue = c.now(), 
            void 0 === t && (n._remainingTimeout = e.timeout), n.paused || n.hoverPaused || (n.timeoutId = setTimeout(function() {
                n.API.prepareTx(!1, !n.reverse);
            }, i))));
        },
        stopTransition: function() {
            var e = this.opts();
            e.slides.filter(":animated").length && (e.slides.stop(!1, !0), e.API.trigger("cycle-transition-stopped", [ e ])), 
            e._tx && e._tx.stopTransition && e._tx.stopTransition(e);
        },
        advanceSlide: function(e) {
            var t = this.opts();
            return clearTimeout(t.timeoutId), t.timeoutId = 0, t.nextSlide = t.currSlide + e, 
            t.nextSlide < 0 ? t.nextSlide = t.slides.length - 1 : t.nextSlide >= t.slides.length && (t.nextSlide = 0), 
            t.API.prepareTx(!0, e >= 0), !1;
        },
        buildSlideOpts: function(e) {
            var t, n, i = this.opts(), r = e.data() || {};
            for (var o in r) r.hasOwnProperty(o) && /^cycle[A-Z]+/.test(o) && (t = r[o], n = o.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, l), 
            i.API.log("[" + (i.slideCount - 1) + "]", n + ":", t, "(" + typeof t + ")"), r[n] = t);
            r = c.extend({}, c.fn.cycle.defaults, i, r), r.slideNum = i.slideCount;
            try {
                delete r.API, delete r.slideCount, delete r.currSlide, delete r.nextSlide, delete r.slides;
            } catch (e) {}
            return r;
        },
        getSlideOpts: function(e) {
            var t = this.opts();
            void 0 === e && (e = t.currSlide);
            var n = t.slides[e], i = c(n).data("cycle.opts");
            return c.extend({}, t, i);
        },
        initSlide: function(e, t, n) {
            var i = this.opts();
            t.css(e.slideCss || {}), n > 0 && t.css("zIndex", n), isNaN(e.speed) && (e.speed = c.fx.speeds[e.speed] || c.fx.speeds._default), 
            e.sync || (e.speed = e.speed / 2), t.addClass(i.slideClass);
        },
        updateView: function(e, t) {
            var n = this.opts();
            if (n._initialized) {
                var i = n.API.getSlideOpts(), r = n.slides[n.currSlide];
                !e && t !== !0 && (n.API.trigger("cycle-update-view-before", [ n, i, r ]), n.updateView < 0) || (n.slideActiveClass && n.slides.removeClass(n.slideActiveClass).eq(n.currSlide).addClass(n.slideActiveClass), 
                e && n.hideNonActive && n.slides.filter(":not(." + n.slideActiveClass + ")").css("visibility", "hidden"), 
                0 === n.updateView && setTimeout(function() {
                    n.API.trigger("cycle-update-view", [ n, i, r, e ]);
                }, i.speed / (n.sync ? 2 : 1)), 0 !== n.updateView && n.API.trigger("cycle-update-view", [ n, i, r, e ]), 
                e && n.API.trigger("cycle-update-view-after", [ n, i, r ]));
            }
        },
        getComponent: function(e) {
            var t = this.opts(), n = t[e];
            return "string" == typeof n ? /^\s*[\>|\+|~]/.test(n) ? t.container.find(n) : c(n) : n.jquery ? n : c(n);
        },
        stackSlides: function(e, t, n) {
            var i = this.opts();
            e || (e = i.slides[i.currSlide], t = i.slides[i.nextSlide], n = !i.reverse), c(e).css("zIndex", i.maxZ);
            var r, o = i.maxZ - 2, s = i.slideCount;
            if (n) {
                for (r = i.currSlide + 1; s > r; r++) c(i.slides[r]).css("zIndex", o--);
                for (r = 0; r < i.currSlide; r++) c(i.slides[r]).css("zIndex", o--);
            } else {
                for (r = i.currSlide - 1; r >= 0; r--) c(i.slides[r]).css("zIndex", o--);
                for (r = s - 1; r > i.currSlide; r--) c(i.slides[r]).css("zIndex", o--);
            }
            c(t).css("zIndex", i.maxZ - 1);
        },
        getSlideIndex: function(e) {
            return this.opts().slides.index(e);
        }
    }, c.fn.cycle.log = function() {
        window.console && console.log && console.log("[cycle2] " + Array.prototype.join.call(arguments, " "));
    }, c.fn.cycle.version = function() {
        return "Cycle2: " + e;
    }, c.fn.cycle.transitions = {
        custom: {},
        none: {
            before: function(e, t, n, i) {
                e.API.stackSlides(n, t, i), e.cssBefore = {
                    opacity: 1,
                    visibility: "visible",
                    display: "block"
                };
            }
        },
        fade: {
            before: function(e, t, n, i) {
                var r = e.API.getSlideOpts(e.nextSlide).slideCss || {};
                e.API.stackSlides(t, n, i), e.cssBefore = c.extend(r, {
                    opacity: 0,
                    visibility: "visible",
                    display: "block"
                }), e.animIn = {
                    opacity: 1
                }, e.animOut = {
                    opacity: 0
                };
            }
        },
        fadeout: {
            before: function(e, t, n, i) {
                var r = e.API.getSlideOpts(e.nextSlide).slideCss || {};
                e.API.stackSlides(t, n, i), e.cssBefore = c.extend(r, {
                    opacity: 1,
                    visibility: "visible",
                    display: "block"
                }), e.animOut = {
                    opacity: 0
                };
            }
        },
        scrollHorz: {
            before: function(e, t, n, i) {
                e.API.stackSlides(t, n, i);
                var r = e.container.css("overflow", "hidden").width();
                e.cssBefore = {
                    left: i ? r : -r,
                    top: 0,
                    opacity: 1,
                    visibility: "visible",
                    display: "block"
                }, e.cssAfter = {
                    zIndex: e._maxZ - 2,
                    left: 0
                }, e.animIn = {
                    left: 0
                }, e.animOut = {
                    left: i ? -r : r
                };
            }
        }
    }, c.fn.cycle.defaults = {
        allowWrap: !0,
        autoSelector: ".cycle-slideshow[data-cycle-auto-init!=false]",
        delay: 0,
        easing: null,
        fx: "fade",
        hideNonActive: !0,
        loop: 0,
        manualFx: void 0,
        manualSpeed: void 0,
        manualTrump: !0,
        maxZ: 100,
        pauseOnHover: !1,
        reverse: !1,
        slideActiveClass: "cycle-slide-active",
        slideClass: "cycle-slide",
        slideCss: {
            position: "absolute",
            top: 0,
            left: 0
        },
        slides: "> img",
        speed: 500,
        startingSlide: 0,
        sync: !0,
        timeout: 4e3,
        updateView: 0
    }, c(document).ready(function() {
        c(c.fn.cycle.defaults.autoSelector).cycle();
    });
}(jQuery), function(a) {
    "use strict";
    function l(e, t) {
        var n, i, r, o = t.autoHeight;
        if ("container" == o) i = a(t.slides[t.currSlide]).outerHeight(), t.container.height(i); else if (t._autoHeightRatio) t.container.height(t.container.width() / t._autoHeightRatio); else if ("calc" === o || "number" == a.type(o) && o >= 0) {
            if (r = "calc" === o ? s(e, t) : o >= t.slides.length ? 0 : o, r == t._sentinelIndex) return;
            t._sentinelIndex = r, t._sentinel && t._sentinel.remove(), n = a(t.slides[r].cloneNode(!0)), 
            n.removeAttr("id name rel").find("[id],[name],[rel]").removeAttr("id name rel"), 
            n.css({
                position: "static",
                visibility: "hidden",
                display: "block"
            }).prependTo(t.container).addClass("cycle-sentinel cycle-slide").removeClass("cycle-slide-active"), 
            n.find("*").css("visibility", "hidden"), t._sentinel = n;
        }
    }
    function s(e, t) {
        var n = 0, i = -1;
        return t.slides.each(function(e) {
            var t = a(this).height();
            t > i && (i = t, n = e);
        }), n;
    }
    function c(e, t, n, i) {
        var r = a(i).outerHeight();
        t.container.animate({
            height: r
        }, t.autoHeightSpeed, t.autoHeightEasing);
    }
    function u(e, t) {
        t._autoHeightOnResize && (a(window).off("resize orientationchange", t._autoHeightOnResize), 
        t._autoHeightOnResize = null), t.container.off("cycle-slide-added cycle-slide-removed", l), 
        t.container.off("cycle-destroyed", u), t.container.off("cycle-before", c), t._sentinel && (t._sentinel.remove(), 
        t._sentinel = null);
    }
    a.extend(a.fn.cycle.defaults, {
        autoHeight: 0,
        autoHeightSpeed: 250,
        autoHeightEasing: null
    }), a(document).on("cycle-initialized", function(e, t) {
        function n() {
            l(e, t);
        }
        var i, r = t.autoHeight, o = a.type(r), s = null;
        ("string" === o || "number" === o) && (t.container.on("cycle-slide-added cycle-slide-removed", l), 
        t.container.on("cycle-destroyed", u), "container" == r ? t.container.on("cycle-before", c) : "string" === o && /\d+\:\d+/.test(r) && (i = r.match(/(\d+)\:(\d+)/), 
        i = i[1] / i[2], t._autoHeightRatio = i), "number" !== o && (t._autoHeightOnResize = function() {
            clearTimeout(s), s = setTimeout(n, 50);
        }, a(window).on("resize orientationchange", t._autoHeightOnResize)), setTimeout(n, 30));
    });
}(jQuery), function(t) {
    "use strict";
    t.extend(t.fn.cycle.defaults, {
        caption: "> .cycle-caption",
        captionTemplate: "{{slideNum}} / {{slideCount}}",
        overlay: "> .cycle-overlay",
        overlayTemplate: "<div>{{title}}</div><div>{{desc}}</div>",
        captionModule: "caption"
    }), t(document).on("cycle-update-view", function(e, i, r, o) {
        if ("caption" === i.captionModule) {
            t.each([ "caption", "overlay" ], function() {
                var e = this, t = r[e + "Template"], n = i.API.getComponent(e);
                n.length && t ? (n.html(i.API.tmpl(t, r, i, o)), n.show()) : n.hide();
            });
        }
    }), t(document).on("cycle-destroyed", function(e, n) {
        var i;
        t.each([ "caption", "overlay" ], function() {
            var e = this, t = n[e + "Template"];
            n[e] && t && (i = n.API.getComponent("caption"), i.empty());
        });
    });
}(jQuery), function(a) {
    "use strict";
    var s = a.fn.cycle;
    a.fn.cycle = function(t) {
        var n, i, r, o = a.makeArray(arguments);
        return "number" == a.type(t) ? this.cycle("goto", t) : "string" == a.type(t) ? this.each(function() {
            var e;
            return n = t, r = a(this).data("cycle.opts"), void 0 === r ? void s.log('slideshow must be initialized before sending commands; "' + n + '" ignored') : (n = "goto" == n ? "jump" : n, 
            i = r.API[n], a.isFunction(i) ? (e = a.makeArray(o), e.shift(), i.apply(r.API, e)) : void s.log("unknown command: ", n));
        }) : s.apply(this, arguments);
    }, a.extend(a.fn.cycle, s), a.extend(s.API, {
        next: function() {
            var e = this.opts();
            if (!e.busy || e.manualTrump) {
                var t = e.reverse ? -1 : 1;
                e.allowWrap === !1 && e.currSlide + t >= e.slideCount || (e.API.advanceSlide(t), 
                e.API.trigger("cycle-next", [ e ]).log("cycle-next"));
            }
        },
        prev: function() {
            var e = this.opts();
            if (!e.busy || e.manualTrump) {
                var t = e.reverse ? 1 : -1;
                e.allowWrap === !1 && e.currSlide + t < 0 || (e.API.advanceSlide(t), e.API.trigger("cycle-prev", [ e ]).log("cycle-prev"));
            }
        },
        destroy: function() {
            this.stop();
            var t = this.opts(), n = a.isFunction(a._data) ? a._data : a.noop;
            clearTimeout(t.timeoutId), t.timeoutId = 0, t.API.stop(), t.API.trigger("cycle-destroyed", [ t ]).log("cycle-destroyed"), 
            t.container.removeData(), n(t.container[0], "parsedAttrs", !1), t.retainStylesOnDestroy || (t.container.removeAttr("style"), 
            t.slides.removeAttr("style"), t.slides.removeClass(t.slideActiveClass)), t.slides.each(function() {
                var e = a(this);
                e.removeData(), e.removeClass(t.slideClass), n(this, "parsedAttrs", !1);
            });
        },
        jump: function(e, t) {
            var n, i = this.opts();
            if (!i.busy || i.manualTrump) {
                var r = parseInt(e, 10);
                if (isNaN(r) || 0 > r || r >= i.slides.length) return void i.API.log("goto: invalid slide index: " + r);
                if (r == i.currSlide) return void i.API.log("goto: skipping, already on slide", r);
                i.nextSlide = r, clearTimeout(i.timeoutId), i.timeoutId = 0, i.API.log("goto: ", r, " (zero-index)"), 
                n = i.currSlide < i.nextSlide, i._tempFx = t, i.API.prepareTx(!0, n);
            }
        },
        stop: function() {
            var e = this.opts(), t = e.container;
            clearTimeout(e.timeoutId), e.timeoutId = 0, e.API.stopTransition(), e.pauseOnHover && (e.pauseOnHover !== !0 && (t = a(e.pauseOnHover)), 
            t.off("mouseenter mouseleave")), e.API.trigger("cycle-stopped", [ e ]).log("cycle-stopped");
        },
        reinit: function() {
            var e = this.opts();
            e.API.destroy(), e.container.cycle();
        },
        remove: function(e) {
            for (var t, n, i = this.opts(), r = [], o = 1, s = 0; s < i.slides.length; s++) t = i.slides[s], 
            s == e ? n = t : (r.push(t), a(t).data("cycle.opts").slideNum = o, o++);
            n && (i.slides = a(r), i.slideCount--, a(n).remove(), e == i.currSlide ? i.API.advanceSlide(1) : e < i.currSlide ? i.currSlide-- : i.currSlide++, 
            i.API.trigger("cycle-slide-removed", [ i, e, n ]).log("cycle-slide-removed"), i.API.updateView());
        }
    }), a(document).on("click.cycle", "[data-cycle-cmd]", function(e) {
        e.preventDefault();
        var t = a(this), n = t.data("cycle-cmd"), i = t.data("cycle-context") || ".cycle-slideshow";
        a(i).cycle(n, t.data("cycle-arg"));
    });
}(jQuery), function(o) {
    "use strict";
    function n(n, i) {
        var r;
        return n._hashFence ? void (n._hashFence = !1) : (r = window.location.hash.substring(1), 
        void n.slides.each(function(e) {
            if (o(this).data("cycle-hash") == r) {
                if (i === !0) n.startingSlide = e; else {
                    var t = n.currSlide < e;
                    n.nextSlide = e, n.API.prepareTx(!0, t);
                }
                return !1;
            }
        }));
    }
    o(document).on("cycle-pre-initialize", function(e, t) {
        n(t, !0), t._onHashChange = function() {
            n(t, !1);
        }, o(window).on("hashchange", t._onHashChange);
    }), o(document).on("cycle-update-view", function(e, t, n) {
        n.hash && "#" + n.hash != window.location.hash && (t._hashFence = !0, window.location.hash = n.hash);
    }), o(document).on("cycle-destroyed", function(e, t) {
        t._onHashChange && o(window).off("hashchange", t._onHashChange);
    });
}(jQuery), function(d) {
    "use strict";
    d.extend(d.fn.cycle.defaults, {
        loader: !1
    }), d(document).on("cycle-bootstrap", function(e, c) {
        function t(e, o) {
            function s(e) {
                var t;
                "wait" == c.loader ? (a.push(e), 0 === l && (a.sort(n), u.apply(c.API, [ a, o ]), 
                c.container.removeClass("cycle-loading"))) : (t = d(c.slides[c.currSlide]), u.apply(c.API, [ e, o ]), 
                t.show(), c.container.removeClass("cycle-loading"));
            }
            function n(e, t) {
                return e.data("index") - t.data("index");
            }
            var a = [];
            if ("string" == d.type(e)) e = d.trim(e); else if ("array" === d.type(e)) for (var t = 0; t < e.length; t++) e[t] = d(e[t])[0];
            e = d(e);
            var l = e.length;
            l && (e.css("visibility", "hidden").appendTo("body").each(function(e) {
                function t() {
                    0 === --n && (--l, s(i));
                }
                var n = 0, i = d(this), r = i.is("img") ? i : i.find("img");
                return i.data("index", e), r = r.filter(":not(.cycle-loader-ignore)").filter(':not([src=""])'), 
                r.length ? (n = r.length, void r.each(function() {
                    this.complete ? t() : d(this).load(function() {
                        t();
                    }).on("error", function() {
                        0 === --n && (c.API.log("slide skipped; img not loaded:", this.src), 0 === --l && "wait" == c.loader && u.apply(c.API, [ a, o ]));
                    });
                })) : (--l, void a.push(i));
            }), l && c.container.addClass("cycle-loading"));
        }
        var u;
        c.loader && (u = c.API.add, c.API.add = t);
    });
}(jQuery), function(s) {
    "use strict";
    function i(n, i, r) {
        var o, e = n.API.getComponent("pager");
        e.each(function() {
            var t = s(this);
            if (i.pagerTemplate) {
                var e = n.API.tmpl(i.pagerTemplate, i, n, r[0]);
                o = s(e).appendTo(t);
            } else o = t.children().eq(n.slideCount - 1);
            o.on(n.pagerEvent, function(e) {
                n.pagerEventBubble || e.preventDefault(), n.API.page(t, e.currentTarget);
            });
        });
    }
    function r(e, t) {
        var n = this.opts();
        if (!n.busy || n.manualTrump) {
            var i = e.children().index(t), r = i, o = n.currSlide < r;
            n.currSlide != r && (n.nextSlide = r, n._tempFx = n.pagerFx, n.API.prepareTx(!0, o), 
            n.API.trigger("cycle-pager-activated", [ n, e, t ]));
        }
    }
    s.extend(s.fn.cycle.defaults, {
        pager: "> .cycle-pager",
        pagerActiveClass: "cycle-pager-active",
        pagerEvent: "click.cycle",
        pagerEventBubble: void 0,
        pagerTemplate: "<span>&bull;</span>"
    }), s(document).on("cycle-bootstrap", function(e, t, n) {
        n.buildPagerLink = i;
    }), s(document).on("cycle-slide-added", function(e, t, n, i) {
        t.pager && (t.API.buildPagerLink(t, n, i), t.API.page = r);
    }), s(document).on("cycle-slide-removed", function(e, t, n) {
        if (t.pager) {
            var i = t.API.getComponent("pager");
            i.each(function() {
                var e = s(this);
                s(e.children()[n]).remove();
            });
        }
    }), s(document).on("cycle-update-view", function(e, t) {
        var n;
        t.pager && (n = t.API.getComponent("pager"), n.each(function() {
            s(this).children().removeClass(t.pagerActiveClass).eq(t.currSlide).addClass(t.pagerActiveClass);
        }));
    }), s(document).on("cycle-destroyed", function(e, t) {
        var n = t.API.getComponent("pager");
        n && (n.children().off(t.pagerEvent), t.pagerTemplate && n.empty());
    });
}(jQuery), function(e) {
    "use strict";
    e.extend(e.fn.cycle.defaults, {
        next: "> .cycle-next",
        nextEvent: "click.cycle",
        disabledClass: "disabled",
        prev: "> .cycle-prev",
        prevEvent: "click.cycle",
        swipe: !1
    }), e(document).on("cycle-initialized", function(e, t) {
        if (t.API.getComponent("next").on(t.nextEvent, function(e) {
            e.preventDefault(), t.API.next();
        }), t.API.getComponent("prev").on(t.prevEvent, function(e) {
            e.preventDefault(), t.API.prev();
        }), t.swipe) {
            var n = t.swipeVert ? "swipeUp.cycle" : "swipeLeft.cycle swipeleft.cycle", i = t.swipeVert ? "swipeDown.cycle" : "swipeRight.cycle swiperight.cycle";
            t.container.on(n, function() {
                t._tempFx = t.swipeFx, t.API.next();
            }), t.container.on(i, function() {
                t._tempFx = t.swipeFx, t.API.prev();
            });
        }
    }), e(document).on("cycle-update-view", function(e, t) {
        if (!t.allowWrap) {
            var n = t.disabledClass, i = t.API.getComponent("next"), r = t.API.getComponent("prev"), o = t._prevBoundry || 0, s = void 0 !== t._nextBoundry ? t._nextBoundry : t.slideCount - 1;
            t.currSlide == s ? i.addClass(n).prop("disabled", !0) : i.removeClass(n).prop("disabled", !1), 
            t.currSlide === o ? r.addClass(n).prop("disabled", !0) : r.removeClass(n).prop("disabled", !1);
        }
    }), e(document).on("cycle-destroyed", function(e, t) {
        t.API.getComponent("prev").off(t.nextEvent), t.API.getComponent("next").off(t.prevEvent), 
        t.container.off("swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle");
    });
}(jQuery), function(c) {
    "use strict";
    c.extend(c.fn.cycle.defaults, {
        progressive: !1
    }), c(document).on("cycle-pre-initialize", function(e, r) {
        if (r.progressive) {
            var o, t, n = r.API, i = n.next, s = n.prev, a = n.prepareTx, l = c.type(r.progressive);
            if ("array" == l) o = r.progressive; else if (c.isFunction(r.progressive)) o = r.progressive(r); else if ("string" == l) {
                if (t = c(r.progressive), o = c.trim(t.html()), !o) return;
                if (/^(\[)/.test(o)) try {
                    o = c.parseJSON(o);
                } catch (e) {
                    return void n.log("error parsing progressive slides", e);
                } else o = o.split(new RegExp(t.data("cycle-split") || "\n")), o[o.length - 1] || o.pop();
            }
            a && (n.prepareTx = function(e, t) {
                var n, i;
                return e || 0 === o.length ? void a.apply(r.API, [ e, t ]) : void (t && r.currSlide == r.slideCount - 1 ? (i = o[0], 
                o = o.slice(1), r.container.one("cycle-slide-added", function(e, t) {
                    setTimeout(function() {
                        t.API.advanceSlide(1);
                    }, 50);
                }), r.API.add(i)) : t || 0 !== r.currSlide ? a.apply(r.API, [ e, t ]) : (n = o.length - 1, 
                i = o[n], o = o.slice(0, n), r.container.one("cycle-slide-added", function(e, t) {
                    setTimeout(function() {
                        t.currSlide = 1, t.API.advanceSlide(-1);
                    }, 50);
                }), r.API.add(i, !0)));
            }), i && (n.next = function() {
                var e = this.opts();
                if (o.length && e.currSlide == e.slideCount - 1) {
                    var t = o[0];
                    o = o.slice(1), e.container.one("cycle-slide-added", function(e, t) {
                        i.apply(t.API), t.container.removeClass("cycle-loading");
                    }), e.container.addClass("cycle-loading"), e.API.add(t);
                } else i.apply(e.API);
            }), s && (n.prev = function() {
                var e = this.opts();
                if (o.length && 0 === e.currSlide) {
                    var t = o.length - 1, n = o[t];
                    o = o.slice(0, t), e.container.one("cycle-slide-added", function(e, t) {
                        t.currSlide = 1, t.API.advanceSlide(-1), t.container.removeClass("cycle-loading");
                    }), e.container.addClass("cycle-loading"), e.API.add(n, !0);
                } else s.apply(e.API);
            });
        }
    });
}(jQuery), function(l) {
    "use strict";
    l.extend(l.fn.cycle.defaults, {
        tmplRegex: "{{((.)?.*?)}}"
    }), l.extend(l.fn.cycle.API, {
        tmpl: function(e, t) {
            var n = new RegExp(t.tmplRegex || l.fn.cycle.defaults.tmplRegex, "g"), a = l.makeArray(arguments);
            return a.shift(), e.replace(n, function(e, t) {
                var n, i, r, o, s = t.split(".");
                for (n = 0; n < a.length; n++) if (r = a[n]) {
                    if (s.length > 1) for (o = r, i = 0; i < s.length; i++) r = o, o = o[s[i]] || t; else o = r[t];
                    if (l.isFunction(o)) return o.apply(r, a);
                    if (void 0 !== o && null !== o && o != t) return o;
                }
                return t;
            });
        }
    });
}(jQuery);

!function(f) {
    "use strict";
    f(document).on("cycle-bootstrap", function(e, t, n) {
        "carousel" === t.fx && (n.getSlideIndex = function(e) {
            var t = this.opts()._carouselWrap.children(), n = t.index(e);
            return n % t.length;
        }, n.next = function() {
            var e = t.reverse ? -1 : 1;
            t.allowWrap === !1 && t.currSlide + e > t.slideCount - t.carouselVisible || (t.API.advanceSlide(e), 
            t.API.trigger("cycle-next", [ t ]).log("cycle-next"));
        });
    }), f.fn.cycle.transitions.carousel = {
        preInit: function(e) {
            e.hideNonActive = !1, e.container.on("cycle-destroyed", f.proxy(this.onDestroy, e.API)), 
            e.API.stopTransition = this.stopTransition;
            for (var t = 0; t < e.startingSlide; t++) e.container.append(e.slides[0]);
        },
        postInit: function(e) {
            var t, n, i, r, o = e.carouselVertical;
            e.carouselVisible && e.carouselVisible > e.slideCount && (e.carouselVisible = e.slideCount - 1);
            var s = e.carouselVisible || e.slides.length, a = {
                display: o ? "block" : "inline-block",
                position: "static"
            };
            if (e.container.css({
                position: "relative",
                overflow: "hidden"
            }), e.slides.css(a), e._currSlide = e.currSlide, r = f('<div class="cycle-carousel-wrap"></div>').prependTo(e.container).css({
                margin: 0,
                padding: 0,
                top: 0,
                left: 0,
                position: "absolute"
            }).append(e.slides), e._carouselWrap = r, o || r.css("white-space", "nowrap"), e.allowWrap !== !1) {
                for (n = 0; n < (void 0 === e.carouselVisible ? 2 : 1); n++) {
                    for (t = 0; t < e.slideCount; t++) r.append(e.slides[t].cloneNode(!0));
                    for (t = e.slideCount; t--; ) r.prepend(e.slides[t].cloneNode(!0));
                }
                r.find(".cycle-slide-active").removeClass("cycle-slide-active"), e.slides.eq(e.startingSlide).addClass("cycle-slide-active");
            }
            e.pager && e.allowWrap === !1 && (i = e.slideCount - s, f(e.pager).children().filter(":gt(" + i + ")").hide()), 
            e._nextBoundry = e.slideCount - e.carouselVisible, this.prepareDimensions(e);
        },
        prepareDimensions: function(e) {
            var t, n, i, r, o = e.carouselVertical, s = e.carouselVisible || e.slides.length;
            if (e.carouselFluid && e.carouselVisible ? e._carouselResizeThrottle || this.fluidSlides(e) : e.carouselVisible && e.carouselSlideDimension ? (t = s * e.carouselSlideDimension, 
            e.container[o ? "height" : "width"](t)) : e.carouselVisible && (t = s * f(e.slides[0])[o ? "outerHeight" : "outerWidth"](!0), 
            e.container[o ? "height" : "width"](t)), n = e.carouselOffset || 0, e.allowWrap !== !1) if (e.carouselSlideDimension) n -= (e.slideCount + e.currSlide) * e.carouselSlideDimension; else for (i = e._carouselWrap.children(), 
            r = 0; r < e.slideCount + e.currSlide; r++) n -= f(i[r])[o ? "outerHeight" : "outerWidth"](!0);
            e._carouselWrap.css(o ? "top" : "left", n);
        },
        fluidSlides: function(t) {
            function e() {
                clearTimeout(i), i = setTimeout(n, 20);
            }
            function n() {
                t._carouselWrap.stop(!1, !0);
                var e = t.container.width() / t.carouselVisible;
                e = Math.ceil(e - o), t._carouselWrap.children().width(e), t._sentinel && t._sentinel.width(e), 
                s(t);
            }
            var i, r = t.slides.eq(0), o = r.outerWidth() - r.width(), s = this.prepareDimensions;
            f(window).on("resize", e), t._carouselResizeThrottle = e, n();
        },
        transition: function(e, t, n, i, r) {
            var o, s = {}, a = e.nextSlide - e.currSlide, l = e.carouselVertical, c = e.speed;
            if (e.allowWrap === !1) {
                i = a > 0;
                var u = e._currSlide, d = e.slideCount - e.carouselVisible;
                a > 0 && e.nextSlide > d && u == d ? a = 0 : a > 0 && e.nextSlide > d ? a = e.nextSlide - u - (e.nextSlide - d) : 0 > a && e.currSlide > d && e.nextSlide > d ? a = 0 : 0 > a && e.currSlide > d ? a += e.currSlide - d : u = e.currSlide, 
                o = this.getScroll(e, l, u, a), e.API.opts()._currSlide = e.nextSlide > d ? d : e.nextSlide;
            } else i && 0 === e.nextSlide ? (o = this.getDim(e, e.currSlide, l), r = this.genCallback(e, i, l, r)) : i || e.nextSlide != e.slideCount - 1 ? o = this.getScroll(e, l, e.currSlide, a) : (o = this.getDim(e, e.currSlide, l), 
            r = this.genCallback(e, i, l, r));
            s[l ? "top" : "left"] = i ? "-=" + o : "+=" + o, e.throttleSpeed && (c = o / f(e.slides[0])[l ? "height" : "width"]() * e.speed), 
            e._carouselWrap.animate(s, c, e.easing, r);
        },
        getDim: function(e, t, n) {
            var i = f(e.slides[t]);
            return i[n ? "outerHeight" : "outerWidth"](!0);
        },
        getScroll: function(e, t, n, i) {
            var r, o = 0;
            if (i > 0) for (r = n; n + i > r; r++) o += this.getDim(e, r, t); else for (r = n; r > n + i; r--) o += this.getDim(e, r, t);
            return o;
        },
        genCallback: function(n, e, i, r) {
            return function() {
                var e = f(n.slides[n.nextSlide]).position(), t = 0 - e[i ? "top" : "left"] + (n.carouselOffset || 0);
                n._carouselWrap.css(n.carouselVertical ? "top" : "left", t), r();
            };
        },
        stopTransition: function() {
            var e = this.opts();
            e.slides.stop(!1, !0), e._carouselWrap.stop(!1, !0);
        },
        onDestroy: function() {
            var e = this.opts();
            e._carouselResizeThrottle && f(window).off("resize", e._carouselResizeThrottle), 
            e.slides.prependTo(e.container), e._carouselWrap.remove();
        }
    };
}(jQuery);

GOOGLE_MAPS_API_KEY = "#";

$(function() {
    $(".accordion").on("click", function() {
        $(this).toggleClass("-active");
    });
});

$(function() {
    $(".gallery-holder").cycle({
        speed: 1e3,
        slides: ".gallery-slide",
        fx: "carousel",
        pager: ".gallery-pager",
        carouselVisible: 1,
        carouselFluid: true,
        pagerTemplate: '<a class="pager-item" href=""></a>'
    });
});

$(function() {
    $(".carousel").cycle({
        speed: 1e3,
        slides: ".testimonal",
        fx: "carousel",
        next: ".next",
        prev: ".prev",
        carouselVisible: 1,
        carouselFluid: true
    });
});

$(window).scroll(function() {
    var e = $(".header"), t = $(window).scrollTop();
    if (t >= 100) e.addClass("-sticky"); else e.removeClass("-sticky");
});

(function(e) {
    e(document).ready(function() {
        e(".nav-toggle, .nav-overlay").on("click", function() {
            e(".nav").toggleClass("-closed");
            e(".nav-overlay").toggleClass("-closed");
        });
    });
})(jQuery);

function em2px(e) {
    var t = parseFloat($("body").css("font-size"));
    return t * e;
}

function menuClass() {
    var e = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (e <= em2px(44.44)) {
        $(".nav").addClass("-closed");
        $(".nav-overlay").addClass("-closed");
    } else {
        $(".nav").removeClass("-closed");
        $(".nav-overlay").removeClass("-closed");
    }
}

$(window).resize(function() {
    menuClass();
});

menuClass();

em2px();
//# sourceMappingURL=script.js.map
